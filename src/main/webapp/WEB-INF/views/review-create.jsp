<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>

<t:wrapper>
    <div class="form-register">
        <form method="post" action="${pageContext.request.contextPath}/p/order/reviews/${order.getId()}">
            <h1 class="h3 mb-3 fw-normal"><fmt:message key="review_title" /> #${order.getId()}</h1>
            <c:if test="${errors.hasErrorsByName('comment')}">
                <c:forEach var="error" items="${errors.getErrorsByName('comment')}">
                    <p><c:out value="${error.getDescription()}" /></p>
                </c:forEach>
            </c:if>
            <c:choose>
                <c:when test="${errors.hasErrors()}">
                    <div class="form-floating mb-3">
                        <textarea class="form-control" name="comment" placeholder="Description" id="floatingTextarea2" style="height: 150px">${errors.getOldByName('comment')}</textarea>
                        <label for="floatingTextarea2"><fmt:message key="comment" /></label>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" name="comment" placeholder="Description" id="floatingTextarea2" style="height: 150px"></textarea>
                        <label for="floatingTextarea2"><fmt:message key="comment" /></label>
                    </div>
                </c:otherwise>
            </c:choose>
            <div class="stars-container"></div>
            <button type="submit" class="w-100 btn btn-lg btn-primary"><fmt:message key="submit" /></button>
        </form>
    </div>
</t:wrapper>
