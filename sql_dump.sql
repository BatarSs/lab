CREATE DATABASE  IF NOT EXISTS `epamfinal`;
USE `epamfinal`;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `balance` int NOT NULL DEFAULT '0',
  `role` varchar(100) NOT NULL DEFAULT 'USER',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `avatar` varchar(191) DEFAULT 'default.png',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES 
(1,'Master 1','test@kek.lel','123123',13,'MASTER','2022-01-07 15:50:58','default.png'),
(2,'user 1','user@asd.asd','123123',1122,'USER','2022-01-07 15:50:58','default.png'),
(3,'Master 2','test@test.com','123123',12,'MASTER','2022-01-07 15:50:58','default.png'),
(4,'user 2','user@user.com','123123',1112,'USER','2022-02-03 14:18:11','default.png'),
(5,'manager 1','asd@asd.asd','123123',0,'MANAGER','2022-02-03 19:48:10','default.png'),
(6,'user 3','user2@user.com','Aa123123',0,'USER','2022-02-12 13:24:01','default.png'),
(8,'123123','user@user.com1','Aa123123',33123,'USER','2022-02-16 15:02:33','default.png'),
(9,'Aasdasd','user@user.dssdf','Aa123123',32,'USER','2022-02-16 15:04:44','default.png'),
(10,'sdasd','sdasdas@asdsad.sad','Aa123123',123123,'USER','2022-02-16 15:05:40','default.png'),
(11,'asdasd','user@user.com12312','Aa123123',900,'USER','2022-02-16 16:26:19','default.png');
UNLOCK TABLES;

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'NEW',
  `price` int DEFAULT NULL,
  `master_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_users_idx` (`user_id`),
  KEY `orders_masters_idx` (`master_id`),
  CONSTRAINT `orders_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `orders_masters` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
INSERT INTO `orders` VALUES 
(1,2,'CANCEL',0,1,'2022-01-08 09:29:15','asd'),
(2,2,'DONE',223323,3,'2022-01-08 09:30:37','12123321321'),
(3,2,'CANCEL',11,1,'2022-01-08 09:43:37','asdasd'),
(4,2,'WAITPAID',2,3,'2022-01-08 10:07:54','asdasd'),
(5,2,'CANCEL',NULL,NULL,'2022-01-08 10:07:54','asdasd'),
(6,2,'NEW',NULL,NULL,'2022-01-08 10:07:54','1111'),
(7,2,'CANCEL',0,0,'2022-01-08 15:53:28','123'),
(8,2,'NEW',NULL,NULL,'2022-01-12 13:41:11','2222'),
(9,4,'DONE',123,3,'2022-02-03 19:51:29','test'),
(10,4,'PAID',10000,3,'2022-02-07 14:50:01','kekes'),
(11,4,'WAITPAID',1,3,'2022-02-07 15:50:38','kekes'),
(12,4,'NEW',0,NULL,'2022-02-16 09:16:15','Почините плиз стекло\r\n'),
(13,10,'WAITPAID',1332,1,'2022-02-16 15:06:10','TEst order'),
(14,11,'DONE',1100,1,'2022-02-16 16:26:54','Новый заказ');
UNLOCK TABLES;


DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int NOT NULL,
  `comment` text,
  `stars` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `reviews_orders_idx` (`order_id`),
  CONSTRAINT `reviews_orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `reviews` WRITE;
INSERT INTO `reviews` VALUES 
(1,2,'sadasd',4,'2022-01-31 11:51:00'),
(2,2,' Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст  ',3,'2022-01-31 11:57:05'),
(3,2,'123',4,'2022-01-31 11:57:42'),
(4,2,'asdasd',4,'2022-01-31 11:59:04'),
(5,2,'123312321',4,'2022-02-01 10:54:45'),
(6,2,'kozel ',2,'2022-02-01 18:49:40'),
(7,2,'Тест русс',1,'2022-02-02 11:23:25'),
(8,9,'Nu batya 4o skazat\'\r\nНу батя чё сказать',5,'2022-02-03 20:06:04'),
(9,9,'testtt1111 transaction',3,'2022-02-16 09:14:28'),
(10,14,'Отличная работа ',4,'2022-02-16 16:29:49'),
(11,14,'Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст Длинный текст  ',2,'2022-02-16 16:29:57');
UNLOCK TABLES;

