<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="reviewHelper" class="com.example.lab.helpers.ReviewHelper"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>

<t:wrapper>
    <section class="hero-index">
        <h1 class="text-center hero-index__title">Repair Lab</h1>
        <div class="d-flex mb-0 justify-content-center my-4">
            <c:choose>
                <c:when test="${authUser != null}">
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/p/profile"><fmt:message key="profile" /></a>
                </c:when>
                <c:otherwise>
                    <a class="btn btn-primary me-2" href="${pageContext.request.contextPath}/p/login"><fmt:message key="login" /></a>
                    <a class="btn btn-warning" href="${pageContext.request.contextPath}/p/register"><fmt:message key="register" /></a>
                </c:otherwise>
            </c:choose>
        </div>
    </section>
    <section class="my-4">
        <div class="row">
            <div class="col-md-4">
                <div class="counter darkgreen">
                    <div class="counter-icon">
                        <i class="fa fa-briefcase"></i>
                    </div>
                    <h2><fmt:message key="orders" /></h2>
                    <span class="counter-value">${ordersCount}</span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="counter darkgreen">
                    <div class="counter-icon">
                        <i class="fa fa-cogs"></i>
                    </div>
                    <h2><fmt:message key="masters" /></h2>
                    <span class="counter-value">${mastersCount}</span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="counter">
                    <div class="counter-icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <h2><fmt:message key="users" /></h2>
                    <span class="counter-value">${usersCount}</span>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="customer-feedback py-4">
                <div class="text-center">
                    <h2 class="section-title "><fmt:message key="latest_reviews" /></h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 position-relative">
                        <div class="owl-carousel feedback-slider">
                            <c:forEach var="review" items="${reviews}">
                                <div class="feedback-slider-item">
                                    <img src="${pageContext.request.contextPath}/images/${reviewHelper.getUserAvatar(review)}" class="center-block img-circle" alt="Customer Feedback">
                                    <h3 class="customer-name">${reviewHelper.getUserName(review)}</h3>
                                    <p>${review.getComment()}</p>
                                    <span class="light-bg customer-rating">
                                            ${reviewHelper.getMark(review)}
                                    </span>
                                </div>
                            </c:forEach>
                        </div>
                    </div><!-- /End col -->
                </div><!-- /End row -->
        </div><!-- /End customer-feedback -->
    </section>
</t:wrapper>