package com.example.lab.utils;

import jakarta.servlet.ServletContext;

public class Context {

    private static ServletContext context;

    public static ServletContext getInstance() {
        if(context == null) {
            throw new Error("Context not available");
        }
        return context;
    }

    private Context() {
    }

    public static void setContext(ServletContext cont) {
        context = cont;
    }

    public static void destroyContext() {
        context = null;
    }
}
