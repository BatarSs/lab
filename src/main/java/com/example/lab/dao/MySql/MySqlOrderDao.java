package com.example.lab.dao.MySql;

import com.example.lab.dao.Dao;
import com.example.lab.dao.OrderDao;
import com.example.lab.models.Order;
import com.example.lab.utils.Database;
import com.example.lab.utils.OrderStatus;

import java.sql.*;
import java.util.List;

public class MySqlOrderDao extends Dao<Order> implements OrderDao {

    private static MySqlOrderDao instance;

    public synchronized static MySqlOrderDao getInstance() {
        if (instance == null) {
            instance = new MySqlOrderDao();
        }
        return instance;
    }

    @Override
    public Order getByEmail(Connection connection, String email) throws SQLException {

        List<Order> list = findByFields(connection,
                "SELECT * FROM orders WHERE email = ?",
                email);

        if (list.isEmpty()) {
            throw new SQLException("Order not found");
        }

        return list.get(0);
    }

    @Override
    public Order getById(Connection connection, int id) throws SQLException {

        List<Order> list = findByFields(connection,
                "SELECT * FROM orders WHERE id = ?",
                id);

        if (list.isEmpty()) {
            throw new SQLException("Order not found");
        }

        return list.get(0);
    }

    @Override
    public List<Order> getAll(Connection connection) throws SQLException {
        return getAll(connection, "SELECT * FROM orders ORDER BY id DESC");
    }

    @Override
    public int getAllCount(Connection connection) throws SQLException {
        return countByFields(connection, "SELECT COUNT(*) as count FROM orders");
    }

    @Override
    public List<Order> getAll(Connection connection, int page, int itemsOnPage) throws SQLException {
        return findByFields(connection, "SELECT * FROM orders ORDER BY id DESC LIMIT " + itemsOnPage + " OFFSET " + itemsOnPage * --page);
    }

    @Override
    public List<Order> getByUserId(Connection connection, Integer id) throws SQLException {
        return findByFields(connection, "SELECT * FROM orders WHERE user_id = ? ORDER BY id DESC", id);
    }

    @Override
    public List<Order> getByUserId(Connection connection, Integer id, int page, int itemsOnPage) throws SQLException {
        return findByFields(connection, "SELECT * FROM orders WHERE user_id = ? ORDER BY id DESC LIMIT " + itemsOnPage + " OFFSET " + itemsOnPage * --page, id);
    }

    @Override
    public List<Order> getByMasterId(Connection connection, Integer id) throws SQLException {
        return findByFields(connection, "SELECT * FROM orders WHERE master_id = ? ORDER BY id DESC", id);
    }

    @Override
    public List<Order> getByMasterId(Connection connection, Integer id, int page, int itemsOnPage) throws SQLException {
        return findByFields(connection, "SELECT * FROM orders WHERE master_id = ? ORDER BY id DESC LIMIT " + itemsOnPage + " OFFSET " + itemsOnPage * --page, id);
    }

    @Override
    public int getByMasterIdCount(Connection connection, Integer id) throws SQLException {
        return countByFields(connection, "SELECT COUNT(*) as count FROM orders WHERE master_id = ?", id);
    }

    @Override
    public int getByUserIdCount(Connection connection, Integer id) throws SQLException {
        return countByFields(connection, "SELECT COUNT(*) as count FROM orders WHERE user_id = ?", id);

    }

    @Override
    public int save(Connection connection, Order order) throws SQLException {
        return save(connection,
                "INSERT INTO orders (user_id, status, description, expire_date, master_id, price) VALUES (?, ?, ?, ?, ?, ?)",
                order);

    }

    @Override
    public void update(Connection connection, Order order) throws SQLException {
        super.update(connection,
                "UPDATE orders SET  user_id=? ,status=?, description=?, expire_date=?, master_id=?, price=? WHERE id = ?", order, 6, order.getId());

    }

    @Override
    public void delete(Connection connection, Order order) throws SQLException {
        super.update(connection,
                "DELETE FROM orders WHERE id = ?", order, 1, order.getId());

    }

    @Override
    protected void prepareStatementFromModel(PreparedStatement ps, Order order) throws SQLException {
        ps.setInt(1, order.getUserId());
        ps.setString(2, order.getStatus().name());
        ps.setString(3, order.getDescription());
        if (order.getExpireDate() != null) {
            ps.setString(4, order.getExpireDate());
        } else {
            ps.setNull(4, Types.TIMESTAMP);
        }
        if (order.getMasterId() != null) {
            ps.setInt(5, order.getMasterId());
        } else {
            ps.setNull(5, Types.INTEGER);
        }
        if (order.getPrice() != null) {
            ps.setInt(6, order.getPrice());
        } else {
            ps.setNull(6, Types.INTEGER);
        }
    }

    @Override
    protected Order getModelFromResult(ResultSet rs) throws SQLException {
        Order order = new Order();

        order.setId(rs.getInt("id"));
        order.setUserId(rs.getInt("user_id"));
        order.setPrice(rs.getInt("price"));
        order.setMasterId(rs.getInt("master_id"));
        order.setDescription(rs.getString("description"));
        order.setCreatedAt(rs.getString("created_at"));
        order.setExpireDate(rs.getString("expire_date"));
        order.setStatus(OrderStatus.valueOf(rs.getString("status")));

        return order;
    }
}
