package com.example.lab.helpers;

import com.example.lab.models.Order;
import com.example.lab.models.User;
import com.example.lab.services.OrderService;
import com.example.lab.services.ServiceFactory;

import java.sql.SQLException;
import java.util.List;

public class ManagerStrategy implements RoleStrategy {

    private static final OrderService orderService;

    static {
        orderService = ServiceFactory.getInstance().getOrderService();
    }

    @Override
    public List<Order> getOrders(User user) throws SQLException {
        return orderService.getAll();
    }

    @Override
    public int getOrdersCount(User user) throws SQLException {
        return orderService.getAllCount();
    }

    @Override
    public List<Order> getOrders(User user, int page, int itemsOnPage) throws SQLException {
        return orderService.getAll(page, itemsOnPage);
    }

    @Override
    public String getOrderPage(User user) {
        return "order";
    }

    @Override
    public Order getOrder(User user, int orderId) throws SQLException {
        return orderService.getById(orderId);
    }

    @Override
    public List<User> getMasters(User user) throws SQLException {
        return ServiceFactory.getInstance().getUserService().getMastersByManager(user);
    }
}
