package com.example.lab.validation;

import com.example.lab.validation.validations.*;
import com.example.lab.validation.validators.BaseValidator;
import com.example.lab.validation.validators.Validator;
import jakarta.servlet.http.HttpServletRequest;

public class RegisterUserCheck {

    public static Validator getValidator(HttpServletRequest request) {
        Validator registerValidator = new BaseValidator();
        String locale = request.getAttribute("lang").toString();

        registerValidator.addValidation(new ValidEmailValidation("email", locale , request.getParameter("email")));
        registerValidator.addValidation(new NewUserEmailValidation("email", locale , request.getParameter("email")));
        registerValidator.addValidation(new UserNameValidation("name", locale , request.getParameter("name")));
        registerValidator.addValidation(new PasswordValidation("password", locale , request.getParameter("password")));
        registerValidator.addValidation(new SamePasswordValidation("password", locale , request.getParameter("password"), request.getParameter("password_confirm")));

        return registerValidator;
    }
}
