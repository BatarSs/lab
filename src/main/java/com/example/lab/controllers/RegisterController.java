package com.example.lab.controllers;

import com.example.lab.exception.DBException;
import com.example.lab.models.User;
import com.example.lab.services.ServiceFactory;
import com.example.lab.services.UserService;
import com.example.lab.utils.Role;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.sql.SQLException;

@ControllerAnnotation("/register")
public class RegisterController implements Controller {

    private static final UserService userService;
    private static final String DEFAULT_AVATAR = "default.png";

    static {
        userService = ServiceFactory.getInstance().getUserService();
    }

    @Override
    public String get(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        if (session.getAttribute("errors") != null) {
            request.setAttribute("errors", session.getAttribute("errors"));
            session.removeAttribute("errors");
        }

        return "register";
    }

    @Override
    public String post(HttpServletRequest request, HttpServletResponse response) throws DBException {
        User user = new User();

        user.setEmail(request.getParameter("email"));
        user.setName(request.getParameter("name"));
        user.setPassword(request.getParameter("password"));
        user.setAvatar(DEFAULT_AVATAR);
        user.setRole(Role.USER);

        int userId;

        try {
            userId  = userService.save(user);
        } catch (SQLException exception) {
            throw new DBException("User not created", exception);
        }

        if (userId > 0) {
            HttpSession session = request.getSession();
            user.setId(userId);
            session.setAttribute("authUser", user);
            return "profile";
        }

        return "register";
    }
}
