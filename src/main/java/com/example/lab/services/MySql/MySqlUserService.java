package com.example.lab.services.MySql;

import com.example.lab.dao.DaoFactory;
import com.example.lab.dao.UserDao;
import com.example.lab.models.User;
import com.example.lab.services.UserService;
import com.example.lab.utils.Database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class MySqlUserService implements UserService {

    private final UserDao userDao;

    public MySqlUserService(DaoFactory daoFactory) {
        userDao = daoFactory.getUserDAO();
    }

    @Override
    public  List<User> getAll() throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return userDao.getAll(connection);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public User get(int id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();

            return userDao.get(connection, id);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public User getByEmail(String email) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return userDao.getByEmail(connection, email);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }


    @Override
    public List<User> getMastersByUser(User user) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return userDao.getMastersByUser(connection, user);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<User> getMastersByMaster(User user) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return userDao.getMastersByMaster(connection, user);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<User> getMastersByManager(User user) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return userDao.getMastersByManager(connection, user);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public int save(User user) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return userDao.save(connection, user);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public void update(User user) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            userDao.update(connection, user);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public void delete(User user) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            userDao.delete(connection, user);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }
}
