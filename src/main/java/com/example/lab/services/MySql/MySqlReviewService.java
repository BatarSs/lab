package com.example.lab.services.MySql;

import com.example.lab.dao.DaoFactory;
import com.example.lab.dao.ReviewDao;
import com.example.lab.models.Review;
import com.example.lab.services.ReviewService;
import com.example.lab.utils.Database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class MySqlReviewService implements ReviewService {

    private final ReviewDao reviewDao;

    public MySqlReviewService(DaoFactory daoFactory) {
        reviewDao = daoFactory.getReviewDao();
    }

    @Override
    public List<Review> getAll() throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return reviewDao.getAll(connection);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Review> getAll(int page, int itemsOnPage) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return reviewDao.getAll(connection, page, itemsOnPage);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public Review getById(long id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return reviewDao.getById(connection, id);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public int save(Review review) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return reviewDao.save(connection, review);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public void update(Review review) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            reviewDao.update(connection, review);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public void delete(Review review) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            reviewDao.delete(connection, review);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Review> getByOrderId(Integer id, int page, int itemsOnPage) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return reviewDao.getByOrderId(connection, id, page, itemsOnPage);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Review> getByOrderId(int id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return reviewDao.getByOrderId(connection, id);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Review> getByMasterId(int id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return reviewDao.getByMasterId(connection, id);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Review> getByUserId(int id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return reviewDao.getByUserId(connection, id);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public int getByOrderIdCount(Integer id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return reviewDao.getByOrderIdCount(connection, id);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }
}
