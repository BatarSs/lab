package com.example.lab.dao.MySql;

import com.example.lab.dao.DaoFactory;
import com.example.lab.dao.OrderDao;
import com.example.lab.dao.ReviewDao;
import com.example.lab.dao.UserDao;

public class MySqlDaoFactory extends DaoFactory {

    @Override
    public UserDao getUserDAO() {
        return MySqlUserDao.getInstance();
    }

    @Override
    public OrderDao getOrderDao() {
        return MySqlOrderDao.getInstance();
    }

    @Override
    public ReviewDao getReviewDao() {
        return MySqlReviewDao.getInstance();
    }
}
