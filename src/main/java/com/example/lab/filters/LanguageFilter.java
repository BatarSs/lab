package com.example.lab.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.*;

import java.io.IOException;
import java.util.Arrays;

@WebFilter(urlPatterns = "/p/*")
public class LanguageFilter extends HttpFilter {

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        if (req.getParameter("locale") != null) {
            Cookie cookie = new Cookie("lang", req.getParameter("locale"));
            res.addCookie(cookie);
            res.sendRedirect(req.getRequestURL().toString());
            return;
        }

        if (Arrays.stream(req.getCookies()).noneMatch(v -> v.getName().equals("lang"))) {
            Cookie cookie = new Cookie("lang", "ru");
            res.addCookie(cookie);
        } else {
            req.setAttribute("lang", Arrays.stream(req.getCookies()).filter(v -> v.getName().equals("lang")).findFirst().get().getValue());
        }


        chain.doFilter(req, res);
    }
}
