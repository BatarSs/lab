<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>
<t:wrapper>
    <div class="form-register">
        <form method="post" action="${pageContext.request.contextPath}/p/orders/create">
            <img class="mb-4" src="${pageContext.request.contextPath}/resources/images/logo.png" alt="" width="72" height="57">
            <h1 class="h3 mb-3 fw-normal"><fmt:message key="create_order" /></h1>
            <c:if test="${errors.hasErrorsByName('descriprion')}">
                <c:forEach var="error" items="${errors.getErrorsByName('descriprion')}">
                    <p><c:out value="${error.getDescription()}" /></p>
                </c:forEach>
            </c:if>
            <c:choose>
                <c:when test="${errors.hasErrors()}">
                    <div class="form-floating mb-3">
                        <textarea class="form-control" name="description" placeholder="<fmt:message key="description" />" id="floatingTextarea2" style="height: 150px">${errors.getOldByName('descriprion')}</textarea>
                        <label for="floatingTextarea2"><fmt:message key="description" /></label>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" name="description" placeholder="<fmt:message key="description" />" id="floatingTextarea2" style="height: 150px"></textarea>
                        <label for="floatingTextarea2"><fmt:message key="description" /></label>
                    </div>
                </c:otherwise>
            </c:choose>

            <input type="datetime-local" name="expire_date" class="form-control mb-3" required>

            <button type="submit" class="w-100 btn btn-lg btn-primary"><fmt:message key="submit" /></button>
        </form>
    </div>
</t:wrapper>
