package com.example.lab.validation.validations;

import com.example.lab.models.User;
import com.example.lab.services.ServiceFactory;
import com.example.lab.services.UserService;
import com.example.lab.validation.ValidatorError;

import java.sql.SQLException;

public class LoginValidation extends AbstractValidation implements Validation {

    private final String email;
    private final String password;

    public LoginValidation(String inputName, String locale, String email, String password) {
        super(inputName, locale);
        this.email = email;
        this.password = password;
    }

    @Override
    public void validate() throws SQLException {
        UserService userService = ServiceFactory.getInstance().getUserService();
        User user = userService.getByEmail(email);

        if (user == null) {
            this.errors.add(new ValidatorError("email", bundle.getString("user_not_found")));
            return;
        }

        if(!user.getPassword().equals(password)) {
            this.errors.add(new ValidatorError("email", bundle.getString("invalid_password")));
        }
    }

    @Override
    public String oldValue() {
        return email;
    }
}
