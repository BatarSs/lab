package com.example.lab.controllers;

import com.example.lab.exception.DBException;
import com.example.lab.models.Order;
import com.example.lab.models.User;
import com.example.lab.services.OrderService;
import com.example.lab.services.ServiceFactory;
import com.example.lab.utils.Notification;
import com.example.lab.utils.OrderStatus;
import com.example.lab.utils.Role;
import com.example.lab.validation.validations.OrderDescriptionValidation;
import com.example.lab.validation.validations.UserNameValidation;
import com.example.lab.validation.validations.ValidEmailValidation;
import com.example.lab.validation.validators.BaseValidator;
import com.example.lab.validation.validators.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

@ControllerAnnotation("/orders/create")
public class OrderCreateController implements Controller {

    private static final OrderService orderService;
    private static final Logger LOGGER = LogManager.getLogger(OrderCreateController.class);

    static {
        orderService = ServiceFactory.getInstance().getOrderService();
    }

    @Override
    @AuthAnnotation(roles = {Role.USER})
    public String post(HttpServletRequest request, HttpServletResponse response) throws DBException {

        User user = (User) request.getAttribute("authUser");

        String locale = request.getAttribute("lang").toString();

        Validator validator = new BaseValidator();
        validator.addValidation(new OrderDescriptionValidation("description", locale, request.getParameter("description")));
        validator.validate();

        HttpSession session = request.getSession();

        if (validator.hasErrors()) {
            session.setAttribute("errors", validator);
            return "order-create";
        }

        Order order = new Order();
        order.setDescription(request.getParameter("description"));
        order.setStatus(OrderStatus.NEW);
        order.setExpireDate(request.getParameter("expire_date"));
        order.setUserId(user.getId());

        try {
            orderService.save(order);
        } catch (SQLException exception) {
            LOGGER.error("Order not created",exception);
            throw new DBException("Order not created", exception);
        }

        session.setAttribute("notification", new Notification(Notification.Type.SUCCESS, "Order created"));

        return "orders";
    }

    @Override
    @AuthAnnotation(roles = {Role.USER})
    public String get(HttpServletRequest request, HttpServletResponse response) {
        return "order-create";
    }
}
