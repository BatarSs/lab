package com.example.lab.dao;

import com.example.lab.models.Review;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface ReviewDao {

    List<Review> getAll(Connection connection) throws SQLException;

    List<Review> getAll(Connection connection, int page, int itemsOnPage) throws SQLException;

    Review getById(Connection connection, long id) throws SQLException;

    int save(Connection connection, Review review) throws SQLException;

    void update(Connection connection, Review review) throws SQLException;

    void delete(Connection connection, Review review) throws SQLException;

    List<Review> getByOrderId(Connection connection, Integer id, int page, int itemsOnPage) throws SQLException;

    List<Review> getByOrderId(Connection connection, int id) throws SQLException;

    List<Review> getByMasterId(Connection connection, int id) throws SQLException;

    List<Review> getByUserId(Connection connection, int id) throws SQLException;

    int getByOrderIdCount(Connection connection, Integer id) throws SQLException;
}
