package com.example.lab.controllers;

import com.example.lab.models.User;
import com.example.lab.services.MySql.MySqlOrderService;
import com.example.lab.services.MySql.MySqlServiceFactory;
import com.example.lab.services.MySql.MySqlUserService;
import com.example.lab.services.ServiceFactory;
import com.example.lab.services.UserService;
import com.example.lab.utils.Role;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.*;
import org.apache.commons.dbcp2.ConnectionFactory;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import javax.naming.spi.NamingManager;

import java.io.IOException;
import java.util.Hashtable;

import static org.mockito.Mockito.*;


public class FrontControllerTest {

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession session;
    ServletConfig servletConfig;
    ServletContext ctx;
    RequestDispatcher dispatcher;
    @Mock
    User user;

    public static class MyContextFactory implements InitialContextFactory
    {
        @Override
        public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
            InitialContext mockCtx = mock(InitialContext.class);
            when(mockCtx.lookup("java:comp/env/dao/ServiceFactory")).thenReturn(new MySqlServiceFactory());
            return mockCtx;
        }
    }

    @BeforeEach
    public void setup() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        servletConfig = mock(ServletConfig.class);
        ctx = mock(ServletContext.class);
        dispatcher = mock(RequestDispatcher.class);
        user = mock(User.class);
        System.setProperty("java.naming.factory.initial",
                this.getClass().getCanonicalName() + "$MyContextFactory");
    }

    @Before
    public void setupClass() {

    }

    @Test
    public void testLoginPage() throws Exception {
        String exceptedPath = "/WEB-INF/views/login.jsp";

        when(request.getPathInfo()).thenReturn("/login");
        when(request.getMethod()).thenReturn("GET");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("errors")).thenReturn(null);
        when(servletConfig.getServletContext()).thenReturn(ctx);

        when(ctx.getRequestDispatcher(exceptedPath)).thenReturn(dispatcher);

        FrontController frontController = new FrontController();
        frontController.init(servletConfig);
        frontController.doGet(request, response);

        verify(ctx, times(1)).getRequestDispatcher(exceptedPath);
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void testHasNoPermissions() throws Exception {

        String exceptedPath = "/WEB-INF/views/401.jsp";

        when(request.getPathInfo()).thenReturn("/profile");
        when(request.getMethod()).thenReturn("GET");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("errors")).thenReturn(null);
        when(servletConfig.getServletContext()).thenReturn(ctx);
        when(ctx.getRequestDispatcher(exceptedPath)).thenReturn(dispatcher);

        FrontController frontController = new FrontController();
        frontController.init(servletConfig);
        frontController.doGet(request, response);

        verify(ctx, times(1)).getRequestDispatcher(exceptedPath);
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void testHasYesPermissions() throws Exception {
        String exceptedPath = "/WEB-INF/views/profile.jsp";

        when(request.getPathInfo()).thenReturn("/profile");
        when(request.getMethod()).thenReturn("GET");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("errors")).thenReturn(null);
        when(servletConfig.getServletContext()).thenReturn(ctx);
        when(request.getAttribute("authUser")).thenReturn(user);
        when(ctx.getRequestDispatcher(exceptedPath)).thenReturn(dispatcher);

        FrontController frontController = new FrontController();
        frontController.init(servletConfig);
        frontController.doGet(request, response);

        verify(ctx, times(1)).getRequestDispatcher(exceptedPath);
        verify(dispatcher).forward(request, response);
    }
}
