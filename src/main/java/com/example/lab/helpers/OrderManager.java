package com.example.lab.helpers;

import com.example.lab.exception.DBException;
import com.example.lab.models.Order;
import com.example.lab.models.User;

import java.sql.SQLException;
import java.util.List;

public class OrderManager extends AbstractManager {

    public OrderManager(User user) {
        super(user);
    }

    public List<Order> getOrders() throws DBException {
        try {
            return role.getOrders(this.user);
        } catch (SQLException exception) {
            throw new DBException("Orders not found", exception);
        }
    }

    public int getOrdersCount() throws DBException {
        try {
            return role.getOrdersCount(this.user);
        } catch (SQLException exception) {
            throw new DBException("Orders not found", exception);
        }
    }

    public List<Order> getOrders(int page, int itemsOnPage) throws DBException {
        try {
            return role.getOrders(this.user, page, itemsOnPage);
        } catch (SQLException exception) {
            throw new DBException("Orders not found", exception);
        }
    }

    public String getOrderPage() {
        return role.getOrderPage(this.user);
    }

    public Order getOrder(int orderId) throws DBException {
        try {
            return role.getOrder(this.user, orderId);
        } catch (SQLException exception) {
            throw new DBException("Order not found", exception);
        }
    }

    public List<User> getMasters() throws DBException {
        try {
            return role.getMasters(this.user);
        } catch (SQLException exception) {
            throw new DBException("Masters not found", exception);
        }

    }

}
