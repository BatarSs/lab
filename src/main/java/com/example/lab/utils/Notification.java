package com.example.lab.utils;

import java.util.Objects;

public class Notification {

    Type type;
    String text;

    public Notification(Type type, String text) {
        this.type = type;
        this.text = text;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "type='" + type + '\'' +
                ", text='" + text + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notification that = (Notification) o;
        return type.equals(that.type) && text.equals(that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, text);
    }

    public enum Type {
        SUCCESS, ERROR, WARNING, INFO
    }
}
