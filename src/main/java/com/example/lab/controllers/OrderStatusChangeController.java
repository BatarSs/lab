package com.example.lab.controllers;


import com.example.lab.exception.DBException;
import com.example.lab.helpers.OrderManager;
import com.example.lab.dao.MySql.MySqlUserDao;
import com.example.lab.models.Order;
import com.example.lab.models.User;
import com.example.lab.services.OrderService;
import com.example.lab.services.ServiceFactory;
import com.example.lab.services.UserService;
import com.example.lab.utils.Notification;
import com.example.lab.utils.OrderStatus;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@ControllerAnnotation("/orders/*/status/change")
public class OrderStatusChangeController extends BaseController implements Controller {

    private static final OrderService orderService;
    private static final UserService userService;
    private static final Logger LOGGER = LogManager.getLogger(OrderStatusChangeController.class);

    static {
        orderService = ServiceFactory.getInstance().getOrderService();
        userService = ServiceFactory.getInstance().getUserService();
    }

    @Override
    public String post(HttpServletRequest request, HttpServletResponse response) throws DBException {
        StringBuilder jb = new StringBuilder();
        String line;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception exception) {
            LOGGER.error("Can't read json", exception);
            throw new DBException("Status error", exception);
        }

        JSONObject obj = new JSONObject(jb.toString());

        if (obj.has("status")) {
            User user = (User) request.getAttribute("authUser");

            OrderStatus status = OrderStatus.valueOf(obj.getString("status"));

            Integer[] ids = getIdsFromRequest(request, 1);

            OrderManager orderManager = new OrderManager(user);
            Order order = orderManager.getOrder(ids[0]);

            if(user.isMaster() && status == OrderStatus.WORK && order.getExpireDate() != null) {
                Timestamp   date = Timestamp.valueOf(order.getExpireDate());

                if(date.before(new Date())) {
                    User client = null;
                    try {
                        client = userService.get(order.getUserId());
                    } catch (SQLException exception) {
                        LOGGER.error("Invalid expire date", exception);
                        throw new DBException("Invalid expire date", exception);
                    }
                    client.setBalance(client.getBalance() + order.getPrice() + (order.getPrice() / 100 * 20));
                    return expireResponse(request, response, order, client);
                }
            }

            if(user.isMaster() && status == OrderStatus.DONE && order.getExpireDate() != null) {
                Timestamp   date = Timestamp.valueOf(order.getExpireDate());

                if(date.before(new Date())) {
                    User client = null;
                    try {
                        client = userService.get(order.getUserId());
                    } catch (SQLException exception) {
                        LOGGER.error("Invalid expire date", exception);
                        throw new DBException("Invalid expire date", exception);
                    }
                    client.setBalance(client.getBalance() + (order.getPrice() / 100 * 20));
                    return expireResponse(request, response, order, client);
                }
            }

            if (user.isUser() && status == OrderStatus.PAID) {
                if (user.getBalance() >= order.getPrice()) {
                    user.setBalance(user.getBalance() - order.getPrice());
                    try {
                        ServiceFactory.getInstance().getUserService().update(user);
                    } catch (SQLException exception) {
                        LOGGER.error("User not updated", exception);
                        throw new DBException("User not updated", exception);
                    }
                } else {
                    String jsonResponse = "{\"success\": false, \"message\" : \"Not enough money\"}";

                    request.setAttribute("jsonData", jsonResponse);
                    response.setStatus(400);

                    return "json";
                }
            }

            order.setStatus(status);
            try {
                orderService.update(order);
            } catch (SQLException exception) {
                LOGGER.error("Order not updated", exception);
                throw new DBException("Order not updated", exception);
            }
        }

        String jsonResponse = "{\"success\": true}";

        HttpSession session = request.getSession();
        session.setAttribute("notification", new Notification(Notification.Type.SUCCESS, "Status updated"));

        request.setAttribute("jsonData", jsonResponse);
        response.setStatus(200);

        return "json";
    }

    private String expireResponse(HttpServletRequest request, HttpServletResponse response, Order order, User client) throws DBException {
        order.setStatus(OrderStatus.OVERDUE);

        try {
            userService.update(client);
        } catch (SQLException exception) {
            LOGGER.error("User not updated", exception);
            throw new DBException("User not updated", exception);
        }

        try {
            orderService.update(order);
        } catch (SQLException exception) {
            LOGGER.error("Order not updated", exception);
            throw new DBException("Order not updated", exception);
        }

        String jsonResponse = "{\"success\": true}";

        HttpSession session = request.getSession();
        session.setAttribute("notification", new Notification(Notification.Type.SUCCESS, "Order expired"));

        request.setAttribute("jsonData", jsonResponse);
        response.setStatus(200);

        return "json";
    }
}
