package com.example.lab.utils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public enum Database {
    INSTANCE;

    private DataSource dataSource;

    Database() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource) context.lookup("java:comp/env/jdbc/Lab");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private Connection getConnection() throws SQLException{
        return dataSource.getConnection();
    }

    public Connection openConnection() throws SQLException{
       return getConnection();
    }

    public Connection beginTransaction() throws SQLException{
        Connection connection = getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

    public void closeConnection(Connection connection) throws SQLException {
        if(connection != null) {
            connection.close();
        }
    }

    public void commit(Connection connection) throws SQLException {
        connection.commit();
    }

    public void rollback(Connection connection) throws SQLException {
        if(connection != null) {
            connection.rollback();
        }
    }

    public void endTransaction(Connection connection) throws SQLException {
        if(connection != null) {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {

            }

            closeConnection(connection);
        }
    }
}
