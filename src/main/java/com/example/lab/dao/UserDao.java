package com.example.lab.dao;

import com.example.lab.models.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface UserDao {

    User get(Connection connection, int id) throws SQLException;

    User getByEmail(Connection connection, String email) throws SQLException;

    List<User> getAll(Connection connection) throws SQLException;

    List<User> getMastersByUser(Connection connection, User user) throws SQLException;

    List<User> getMastersByMaster(Connection connection, User user) throws SQLException;

    List<User> getMastersByManager(Connection connection, User user) throws SQLException;

    int save(Connection connection, User user) throws SQLException;

    void update(Connection connection, User user) throws SQLException;

    void delete(Connection connection, User user) throws SQLException;
}
