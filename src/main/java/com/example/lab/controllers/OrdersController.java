package com.example.lab.controllers;


import com.example.lab.exception.DBException;
import com.example.lab.helpers.OrderManager;
import com.example.lab.models.Order;
import com.example.lab.models.User;
import com.example.lab.utils.OrderStatus;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@ControllerAnnotation("/orders")
public class OrdersController implements Controller {

    private static final Logger LOGGER = LogManager.getLogger(OrdersController.class);

    @Override
    @AuthAnnotation
    public String get(HttpServletRequest request, HttpServletResponse response) throws DBException {
        User user = (User) request.getAttribute("authUser");

        Object pageParam = request.getParameter("page");
        int page = 1;
        int itemsOnPage = 3;

        if (pageParam != null) {
            page = Integer.parseInt(pageParam.toString());
            if (page < 1) {
                page = 1;
            }
        }

        OrderManager orderManager = new OrderManager(user);

        int ordersCount = orderManager.getOrdersCount();

        int pages = 0;
        List<Order> orders;
        List<User> masters = orderManager.getMasters();

        String sort = "default";

        if (request.getParameter("sort") != null) {
            sort = request.getParameter("sort");
        }

        if (request.getParameter("master") != null || request.getParameter("status") != null || request.getParameter("sort") != null) {
            orders = orderManager.getOrders();
            if (request.getParameter("master") != null) {
                int masterId = Integer.parseInt(request.getParameter("master"));
                if (masterId > 0) {
                    orders = orders.stream().filter(o -> o.getMasterId() == masterId).collect(Collectors.toList());
                }
            }

            if (request.getParameter("status") != null) {
                OrderStatus status = OrderStatus.get(Integer.parseInt(request.getParameter("status")));
                orders = orders.stream().filter(o -> o.getStatus() == status).collect(Collectors.toList());
            }

            orders = sortOrders(orders, sort);

            pages = orders.size() % itemsOnPage == 0 ? orders.size() / itemsOnPage : orders.size() / itemsOnPage + 1;

            if (orders.size() > itemsOnPage) {
                if (itemsOnPage * (page - 1) + itemsOnPage > orders.size()) {
                    orders = orders.subList(itemsOnPage * (page - 1), orders.size());
                } else {
                    orders = orders.subList(itemsOnPage * (page - 1), itemsOnPage * (page - 1) + itemsOnPage);
                }

            }
        } else {
            pages = ordersCount % itemsOnPage == 0 ? ordersCount / itemsOnPage : ordersCount / itemsOnPage + 1;
            orders = orderManager.getOrders(page, itemsOnPage);
        }

        addStatuses(request);

        request.setAttribute("orders", orders);
        request.setAttribute("page", page);
        request.setAttribute("pages", pages);
        request.setAttribute("masters", masters);
        request.setAttribute("statuses", OrderStatus.values());
        request.setAttribute("request", request);

        return "orders";
    }

    private void addStatuses(HttpServletRequest request) {
        Map<String, String> sorts = new HashMap<>();

        sorts.put("status_asc", "Status from min");
        sorts.put("status_desc", "Status from max");
        sorts.put("date_asc", "Created date from min");
        sorts.put("date_desc", "Created date from max");
        sorts.put("price_asc", "Price from min");
        sorts.put("price_desc", "Price from max");

        request.setAttribute("sorts", sorts);
    }

    private List<Order> sortOrders(List<Order> orders, String sort) {
        switch (sort) {
            case "date_desc":
                orders = orders.stream().sorted(Comparator.comparing(x -> Timestamp.valueOf(x.getCreatedAt()))).collect(Collectors.toList());
                break;
            case "date_asc":
                orders = orders.stream().sorted((x, y) -> Timestamp.valueOf(y.getCreatedAt()).compareTo(Timestamp.valueOf(x.getCreatedAt()))).collect(Collectors.toList());
                break;
            case "price_desc":
                orders = orders.stream().sorted((x, y) -> y.getPrice().compareTo(x.getPrice())).collect(Collectors.toList());
                break;
            case "price_asc":
                orders = orders.stream().sorted(Comparator.comparingInt(Order::getPrice)).collect(Collectors.toList());
                break;
            case "status_desc":
                orders = orders.stream().sorted(Comparator.comparing(Order::getStatus)).collect(Collectors.toList());
                break;
            case "status_asc":
                orders = orders.stream().sorted((x, y) -> y.getStatus().compareTo(x.getStatus())).collect(Collectors.toList());
                break;
            default:
                orders = orders.stream().sorted(Comparator.comparingLong(Order::getId)).collect(Collectors.toList());
        }

        return orders;
    }


}
