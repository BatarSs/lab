package com.example.lab.controllers;

import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.SQLException;

import static org.mockito.Mockito.mock;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


public class BaseControllerTest {

    HttpServletRequest request;
    BaseController baseController;

    @BeforeEach
    public void setup() {
        request = mock(HttpServletRequest.class);
        baseController = mock(BaseController.class, Mockito.CALLS_REAL_METHODS);
    }

    @Test
    public void getIdsTestWithOne() throws IllegalArgumentException {
        when(request.getPathInfo()).thenReturn("/orders/12");

        Integer[] result = baseController.getIdsFromRequest(request, 1);

        assertArrayEquals(new Integer[]{12}, result);
    }

    @Test
    public void getIdsTestWithThree() throws IllegalArgumentException {
        when(request.getPathInfo()).thenReturn("/orders/12/45/11");

        Integer[] result = baseController.getIdsFromRequest(request, 3);

        assertArrayEquals(new Integer[]{12, 45, 11}, result);
    }

    @Test
    public void getIdsTestFail() {
        when(request.getPathInfo()).thenReturn("/orders/sd");

        assertThrows(IllegalArgumentException.class, () ->
                baseController.getIdsFromRequest(request, 1)
        );
    }

    @Test
    public void getIdsTestLessWrongCount() {
        when(request.getPathInfo()).thenReturn("/orders/12/45/11");

        assertThrows(IllegalArgumentException.class, () ->
                baseController.getIdsFromRequest(request, 1)
        );
    }

    @Test
    public void getIdsTestOverWrongCount() {
        when(request.getPathInfo()).thenReturn("/orders/12/45/11");

        assertThrows(IllegalArgumentException.class, () ->
                baseController.getIdsFromRequest(request, 4)
        );
    }

}
