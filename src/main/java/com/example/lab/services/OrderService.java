package com.example.lab.services;

import com.example.lab.models.Order;

import java.sql.SQLException;
import java.util.List;

public interface OrderService {

    Order getByEmail(String email) throws SQLException;

    Order getById(int id) throws SQLException;

    List<Order> getAll() throws SQLException;

    int getAllCount() throws SQLException;

    List<Order> getAll(int page, int itemsOnPage) throws SQLException;

    List<Order> getByUserId(Integer id) throws SQLException;

    List<Order> getByUserId(Integer id, int page, int itemsOnPage) throws SQLException;

    List<Order> getByMasterId(Integer id) throws SQLException;

    List<Order> getByMasterId(Integer id, int page, int itemsOnPage) throws SQLException;

    int getByMasterIdCount(Integer id) throws SQLException;

    int getByUserIdCount(Integer id) throws SQLException;

    int save(Order order) throws SQLException;

    void update(Order order) throws SQLException;

    void delete(Order order) throws SQLException;

}
