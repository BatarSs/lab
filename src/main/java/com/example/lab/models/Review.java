package com.example.lab.models;

import com.example.lab.utils.OrderStatus;

import java.io.Serializable;
import java.util.Objects;

public class Review implements Serializable {

    private int id;
    private int oderId;
    private int userId;
    private int masterId;
    private String comment;
    private int stars;
    private String createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOderId() {
        return oderId;
    }

    public void setOderId(int oderId) {
        this.oderId = oderId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return id == review.id && oderId == review.oderId && stars == review.stars && comment.equals(review.comment) && createdAt.equals(review.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, oderId, comment, stars, createdAt);
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", oderId=" + oderId +
                ", masterId=" + masterId +
                ", userId=" + userId +
                ", comment='" + comment + '\'' +
                ", stars=" + stars +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }

    public void setUserId(int userIid) {
        this.userId = userIid;
    }

    public void setMasterId(int masterId) {
        this.masterId = masterId;
    }

    public int getUserId() {
        return this.userId;
    }

    public int getMasterId() {
        return this.masterId;
    }
}
