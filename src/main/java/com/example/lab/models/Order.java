package com.example.lab.models;

import com.example.lab.utils.OrderStatus;

import java.io.Serializable;
import java.util.Objects;

public class Order implements Serializable {

    private int id;
    private int userId;
    private OrderStatus status;
    private int price;
    private String description;
    private Integer masterId;
    private String createdAt;
    private String expireDate;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Integer getMasterId() {
        return masterId;
    }

    public void setMasterId(Integer masterId) {
        this.masterId = masterId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return getId() == order.getId()
                && getUserId() == order.getUserId()
                && getDescription().equals(order.getDescription())
                && getPrice().equals(order.getPrice())
                && getStatus() == order.getStatus()
                && getMasterId().equals(order.getMasterId())
                && getCreatedAt().equals(order.getCreatedAt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(),
                getUserId(),
                getStatus(),
                getDescription(),
                getPrice(),
                getMasterId(),
                getCreatedAt());
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", userId=" + userId +
                ", status=" + status +
                ", description=" + description +
                ", price=" + price +
                ", masterId=" + masterId +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}
