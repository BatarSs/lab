package com.example.lab.validation.validations;


import com.example.lab.validation.ValidatorError;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidEmailValidation extends AbstractValidation implements Validation {

    private final String email;

    public ValidEmailValidation(String inputName, String locale, String email) {
        super(inputName, locale);
        this.email = email;
    }

    public void validate() {
        if (email == null || email.length() == 0) {
            this.errors.add(new ValidatorError("email", bundle.getString("email_required")));
            return;
        }
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            this.errors.add(new ValidatorError("email", bundle.getString("invalid_email")));
        }
    }

    @Override
    public String oldValue() {
        return email;
    }
}
