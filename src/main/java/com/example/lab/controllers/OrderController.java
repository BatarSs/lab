package com.example.lab.controllers;

import com.example.lab.exception.DBException;
import com.example.lab.helpers.OrderManager;
import com.example.lab.helpers.ReviewManager;
import com.example.lab.models.Order;
import com.example.lab.models.Review;
import com.example.lab.models.User;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

@ControllerAnnotation("/orders/*")
public class OrderController extends BaseController implements Controller {

    private static final Logger LOGGER = LogManager.getLogger(IndexController.class);

    @Override
    @AuthAnnotation
    public String get(HttpServletRequest request, HttpServletResponse response) throws DBException {
        User user = (User) request.getAttribute("authUser");

        Integer[] ids = getIdsFromRequest(request, 1);

        OrderManager orderManager = new OrderManager(user);
        Order order = orderManager.getOrder(ids[0]);

        request.setAttribute("order", order);

        ReviewManager reviewManager = new ReviewManager();
        List<Review> reviews = reviewManager.getReviewsByOrder(order);

        request.setAttribute("reviews", reviews);

        return orderManager.getOrderPage();
    }

    @Override
    @AuthAnnotation
    public String post(HttpServletRequest request, HttpServletResponse response) {
        return null;
    }
}
