<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="orderHelper" class="com.example.lab.helpers.OrderHelper"/>
<jsp:useBean id="userHelper" class="com.example.lab.helpers.UserHelper"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>

<t:wrapper>
        <div class="main-body">
            <div class="row gutters-sm mt-4">
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-column align-items-center text-center">
                                <img src="${pageContext.request.contextPath}/images/${authUser.getAvatar()}" alt="${user.getName()}" class="rounded-circle" width="150">
                                <div class="mt-3">
                                    <h4>${authUser.getName()}</h4>
                                    <p class="text-secondary mb-1">${userHelper.getRoleName(authUser)}</p>
<%--                                    <p class="text-muted font-size-sm">Bay Area, San Francisco, CA</p>--%>
<%--                                    <button class="btn btn-primary">Follow</button>--%>
                                    <c:if test="${authUser.isUser()}">
                                        <a class="btn btn-info mt-4" href="${pageContext.request.contextPath}/p/orders/create"><fmt:message key="order_create" /></a>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-3">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <a href="${pageContext.request.contextPath}/p/orders">
                                    <h6 class="mb-0"><fmt:message key="orders" /></h6>
                                </a>
                                <span class="text-secondary">${orderHelper.getOrdersCount(authUser)}</span>
                            </li>
                            <c:if test="${authUser.isManager()}">
                                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                    <a href="${pageContext.request.contextPath}/p/users">
                                        <h6 class="mb-0"><fmt:message key="users" /></h6>
                                    </a>
                                    <span class="text-secondary">${userHelper.getUsersCount()}</span>
                                </li>
                            </c:if>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0"><fmt:message key="name" /></h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${authUser.getName()}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0"><fmt:message key="email" /></h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                        ${authUser.getEmail()}
                                </div>
                            </div>
                            <c:if test="${authUser.isUser()}">
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h6 class="mb-0"><fmt:message key="balance" /></h6>
                                    </div>
                                    <div class="col-sm-9 text-secondary">
                                            ${authUser.getBalance()}
                                    </div>
                                </div>
                            </c:if>
<%--                            <hr>--%>
<%--                            <div class="row">--%>
<%--                                <div class="col-sm-3">--%>
<%--                                    <h6 class="mb-0">Phone</h6>--%>
<%--                                </div>--%>
<%--                                <div class="col-sm-9 text-secondary">--%>
<%--                                    (239) 816-9029--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <hr>--%>
<%--                            <div class="row">--%>
<%--                                <div class="col-sm-3">--%>
<%--                                    <h6 class="mb-0">Mobile</h6>--%>
<%--                                </div>--%>
<%--                                <div class="col-sm-9 text-secondary">--%>
<%--                                    (320) 380-4539--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <hr>--%>
<%--                            <div class="row">--%>
<%--                                <div class="col-sm-3">--%>
<%--                                    <h6 class="mb-0">Address</h6>--%>
<%--                                </div>--%>
<%--                                <div class="col-sm-9 text-secondary">--%>
<%--                                    Bay Area, San Francisco, CA--%>
<%--                                </div>--%>
<%--                            </div>--%>
                            <hr>
                            <div class="row">
                                <div class="col-6">
                                    <a class="btn btn-info" href="${pageContext.request.contextPath}/p/profile/settings"><fmt:message key="edit" /></a>
                                </div>
                                <div class="col-6 justify-content-end d-flex">
                                    <form method="post" class="mb-0" action="${pageContext.request.contextPath}/p/profile/logout">
                                        <button type="submit" class="btn btn-outline-danger"><fmt:message key="logout" /></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </div>
</t:wrapper>
