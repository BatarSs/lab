package com.example.lab.services;

import com.example.lab.models.Review;

import java.sql.SQLException;
import java.util.List;

public interface ReviewService {
    List<Review> getAll() throws SQLException;

    List<Review> getAll(int page, int itemsOnPage) throws SQLException;

    Review getById(long id) throws SQLException;

    int save(Review review) throws SQLException;

    void update(Review review) throws SQLException;

    void delete(Review review) throws SQLException;

    List<Review> getByOrderId(Integer id, int page, int itemsOnPage) throws SQLException;

    List<Review> getByOrderId(int id) throws SQLException;

    List<Review> getByMasterId(int id) throws SQLException;

    List<Review> getByUserId(int id) throws SQLException;

    int getByOrderIdCount(Integer id) throws SQLException;
}
