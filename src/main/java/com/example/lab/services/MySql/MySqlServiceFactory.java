package com.example.lab.services.MySql;

import com.example.lab.dao.MySql.MySqlDaoFactory;
import com.example.lab.services.OrderService;
import com.example.lab.services.ReviewService;
import com.example.lab.services.ServiceFactory;
import com.example.lab.services.UserService;

public class MySqlServiceFactory extends ServiceFactory {

    static {
        daoFactory = new MySqlDaoFactory();
    }

    @Override
    public UserService getUserService() {
        return new MySqlUserService(daoFactory);
    }

    @Override
    public OrderService getOrderService() {
        return new MySqlOrderService(daoFactory);
    }

    @Override
    public ReviewService getReviewService() {
        return new MySqlReviewService(daoFactory);
    }
}
