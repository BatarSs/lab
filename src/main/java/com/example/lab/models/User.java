package com.example.lab.models;

import com.example.lab.utils.Role;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {

    private int id;
    private String email;
    private String password;
    private String name;
    private int balance;
    private String avatar;
    private Role role;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id
                && balance == user.balance
                && Objects.equals(email, user.email)
                && Objects.equals(password, user.password)
                && Objects.equals(name, user.name)
                && role == user.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, password, name, balance, role);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public boolean isMaster() {
        return this.getRole().toString().equalsIgnoreCase("MASTER");
    }

    public boolean isManager() {
        return this.getRole().toString().equalsIgnoreCase("MANAGER");
    }

    public boolean isUser() {
        return this.getRole().toString().equalsIgnoreCase("USER");
    }
}
