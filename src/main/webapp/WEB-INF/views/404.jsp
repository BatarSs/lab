<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>
<html>
<head>
    <title>Title</title>
    <style>
        /**/
        :root {
            --main-color: #eaeaea;
            --stroke-color: black;
        }
        /**/
        body {
            background: var(--main-color);
        }
        h1 {
            margin: 100px auto 0 auto;
            color: var(--stroke-color);
            font-family: monospace;
            font-size: 10rem; line-height: 10rem;
            font-weight: 200;
            text-align: center;
        }
        h2 {
            margin: 20px auto 30px auto;
            font-family: monospace;
            font-size: 1.5rem;
            font-weight: 200;
            text-align: center;
        }
        h1, h2 {
            -webkit-transition: opacity 0.5s linear, margin-top 0.5s linear; /* Safari */
            transition: opacity 0.5s linear, margin-top 0.5s linear;
        }
        .loading h1, .loading h2 {
            /*margin-top: 0px;*/
            opacity: 0;
        }
        .gears {
            position: relative;
            margin: 0 auto;
            width: auto; height: 0;
        }
        .gear {
            position: relative;
            z-index: 0;
            width: 120px; height: 120px;
            margin: 0 auto;
            border-radius: 50%;
            background: var(--stroke-color);
        }
        .question {
            position: relative;
            z-index: 0;
            width: 120px; height: 120px;
            margin: 0 auto;
            top: -75px;
            text-align: center;
            font-size: 125px;
            font-family: monospace;
        }
        .gear:before{
            position: absolute; left: 5px; top: 5px; right: 5px; bottom: 5px;
            z-index: 2;
            content: "";
            border-radius: 50%;
            background: var(--main-color);
        }
        .gear:after {
            position: absolute; left: 25px; top: 25px;
            z-index: 3;
            content: "";
            width: 70px; height: 70px;
            border-radius: 50%;
            border: 5px solid var(--stroke-color);
            box-sizing: border-box;
            background: var(--main-color);
        }
        .gear.one {
            left: -130px;
        }
        .gear.three {
            top: -235px;
            left: 130px;
        }
        .gear .bar {
            position: absolute; left: -15px; top: 50%;
            z-index: 0;
            width: 150px; height: 30px;
            margin-top: -15px;
            border-radius: 5px;
            background: var(--stroke-color);
        }
        .gear .bar:before {
            position: absolute; left: 5px; top: 5px; right: 5px; bottom: 5px;
            z-index: 1;
            content: "";
            border-radius: 2px;
            background: var(--main-color);
        }
        .gear .bar:nth-child(2) {
            transform: rotate(60deg);
            -webkit-transform: rotate(60deg);
        }
        .gear .bar:nth-child(3) {
            transform: rotate(120deg);
            -webkit-transform: rotate(120deg);
        }
        @-webkit-keyframes clockwise {
            0% { -webkit-transform: rotate(0deg);}
            100% { -webkit-transform: rotate(360deg);}
        }

        .gear.one {
            -webkit-animation: clockwise 2s linear infinite;
        }
    </style>
</head>
<body id="body">
<h1>404</h1>
<h2><fmt:message key="404" /> <b>:(</b></h2>
<div class="gears">
    <div class="gear one">
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
    </div>
    <div class="question">
        <span>?</span>
    </div>
    <div class="gear three">
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
    </div>
</div>
</body>
</html>
