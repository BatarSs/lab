package com.example.lab.controllers;

import com.example.lab.dao.DaoFactory;
import com.example.lab.dao.UserDao;
import com.example.lab.exception.DBException;
import com.example.lab.models.User;
import com.example.lab.services.ServiceFactory;
import com.example.lab.services.UserService;
import com.example.lab.utils.FileSaver;
import com.example.lab.utils.Notification;
import com.example.lab.validation.validations.UserNameValidation;
import com.example.lab.validation.validations.ValidEmailValidation;
import com.example.lab.validation.validators.BaseValidator;
import com.example.lab.validation.validators.Validator;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;

@ControllerAnnotation("/profile/settings")
public class ProfileSettingsController implements Controller {

    private static final UserService userService;

    static {
        userService = ServiceFactory.getInstance().getUserService();
    }

    @Override
    @AuthAnnotation
    public String get(HttpServletRequest request, HttpServletResponse response) {
        return "profile-settings";
    }

    @Override
    @AuthAnnotation
    public String post(HttpServletRequest request, HttpServletResponse response) throws DBException {
        User user = (User) request.getAttribute("authUser");

        String locale = request.getAttribute("lang").toString();

        Validator validator = new BaseValidator();
        validator.addValidation(new ValidEmailValidation("email", locale, request.getParameter("email")));
        validator.addValidation(new UserNameValidation("name", locale, request.getParameter("name")));
        validator.validate();

        HttpSession session = request.getSession();

        if (validator.hasErrors()) {
            session.setAttribute("errors", validator);
            return "profile/settings";
        }

        user.setEmail(request.getParameter("email"));
        user.setName(request.getParameter("name"));
        if (request.getParameter("balance") != null) {
            user.setBalance(Integer.parseInt(request.getParameter("balance")));
        }


        try {
            if (request.getPart("image").getSize() > 0) {
                user.setAvatar(FileSaver.save(request, "image"));
            }
        } catch (IOException | ServletException exception) {
            exception.printStackTrace();
            throw new DBException("File not loaded", exception);
        }

        session.setAttribute("notification", new Notification(Notification.Type.SUCCESS, "Profile updated"));

        try {
            userService.update(user);
        } catch (SQLException exception) {
            throw new DBException("User not updated", exception);
        }

        return "profile/settings";
    }
}
