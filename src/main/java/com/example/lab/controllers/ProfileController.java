package com.example.lab.controllers;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ControllerAnnotation("/profile")
public class ProfileController extends BaseController implements Controller {

    private static final Logger LOGGER = LogManager.getLogger(ProfileController.class);

    @Override
    @AuthAnnotation
    public String get(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.debug(request.getSession().getAttribute("authUser"));

        return "profile";
    }
}
