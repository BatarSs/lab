package com.example.lab.dao;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public abstract class DaoFactory {

    public abstract UserDao getUserDAO();

    public abstract OrderDao getOrderDao();

    public abstract ReviewDao getReviewDao();
}
