package com.example.lab.services.MySql;

import com.example.lab.dao.DaoFactory;
import com.example.lab.dao.OrderDao;
import com.example.lab.models.Order;
import com.example.lab.services.OrderService;
import com.example.lab.utils.Database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class MySqlOrderService implements OrderService {

    private final OrderDao orderDao;

    public MySqlOrderService(DaoFactory daoFactory) {
        orderDao = daoFactory.getOrderDao();
    }

    @Override
    public Order getByEmail(String email) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();

            return orderDao.getByEmail(connection, email);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public Order getById(int id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();

            return orderDao.getById(connection, id);

        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Order> getAll() throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return orderDao.getAll(connection);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public int getAllCount() throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return orderDao.getAllCount(connection);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Order> getAll(int page, int itemsOnPage) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return orderDao.getAll(connection, page, itemsOnPage);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Order> getByUserId(Integer id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return orderDao.getByUserId(connection, id);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Order> getByUserId(Integer id, int page, int itemsOnPage) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return orderDao.getByUserId(connection, id, page, itemsOnPage);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Order> getByMasterId(Integer id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return orderDao.getByMasterId(connection, id);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public List<Order> getByMasterId(Integer id, int page, int itemsOnPage) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return orderDao.getByMasterId(connection, id, page, itemsOnPage);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }

    }

    @Override
    public int getByMasterIdCount(Integer id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return orderDao.getByUserIdCount(connection, id);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public int getByUserIdCount(Integer id) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return orderDao.getByUserIdCount(connection, id);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public int save(Order order) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            return orderDao.save(connection, order);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public void update(Order order) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            orderDao.update(connection, order);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }

    @Override
    public void delete(Order order) throws SQLException {
        Connection connection = null;
        try {
            connection = Database.INSTANCE.openConnection();
            orderDao.update(connection, order);
        } finally {
            Database.INSTANCE.closeConnection(connection);
        }
    }
}
