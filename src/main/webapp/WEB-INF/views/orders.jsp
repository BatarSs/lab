<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="orderHelper" class="com.example.lab.helpers.OrderHelper"/>
<%@ taglib prefix="orderTag" uri="http://www.my.tag/example" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>

<t:wrapper>
    <form class="d-flex row mx-1 filter-form">
        <div class="col-6 col-md-3 my-3">
            <label for="master-select" class="form-label"><fmt:message key="master" /></label>
            <select class="form-select" id="master-select" name="master" aria-label="Default select example">
                <option selected value="any"><fmt:message key="any" /></option>
                <c:forEach var="master" items="${masters}">
                   <option <c:if test="${pageContext.request.getParameter('master') == master.getId()}"> selected </c:if> value="${master.getId()}">${master.getName()}</option>
                </c:forEach>
            </select>
        </div>

        <div class="col-6 col-md-3 my-3">
            <label for="status-select" class="form-label"><fmt:message key="status" /></label>
            <select class="form-select" id="status-select" name="status" aria-label="Default select example">
                <option selected value="any"><fmt:message key="any" /></option>
                <c:forEach var="status" items="${statuses}">
                    <option <c:if test="${pageContext.request.getParameter('status') == status.ordinal()}"> selected </c:if> value="${status.ordinal()}">${status.getName(cookie['lang'].value)}</option>
                </c:forEach>
            </select>
        </div>

        <div class="col-6 col-md-3 my-3">
            <label for="order-select" class="form-label"><fmt:message key="sort" /></label>
            <select class="form-select" id="order-select" name="sort" aria-label="Default select example">
                <option selected value="any"><fmt:message key="any" /></option>
                <c:forEach var="sort" items="${sorts.entrySet()}">
                    <option <c:if test="${pageContext.request.getParameter('sort') == sort.getKey()}"> selected </c:if> value="${sort.getKey()}"><fmt:message key="${sort.getKey()}" /></option>
                </c:forEach>
            </select>
        </div>

        <div class="col-6 col-md-3 my-3 d-flex justify-content-end align-items-end">
            <button class="btn btn-warning"><fmt:message key="search" /></button>
        </div>
    </form>
    <div class="row my-5">
        <c:forEach var="order" items="${orders}">
            <orderTag:OrderTag order="${order}" locale="${cookie['lang'].value}" user="${authUser}" showEdit="${true}" showView="${true}" classes="col col-12 col-sm-6 col-lg-4 col-xl-3"/>
        </c:forEach>
    </div>
    <c:if test="${pages > 0}">
        <nav aria-label="Page navigation example" class="d-flex justify-content-center">
            <ul class="pagination">
                <c:forEach var="i" begin="1" end="${pages}">
                    <c:choose>
                        <c:when test="${page == i}">
                            <li class="page-item active" aria-current="page">
                                <a class="page-link" href="#"><c:out value="${i}"/></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="page-item">
                                <a class="page-link"
                                   href="${orderHelper.getLink(request, i)}"><c:out
                                        value="${i}"/></a>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </nav>
    </c:if>
</t:wrapper>
