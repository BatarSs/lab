package com.example.lab.validation.validations;

import com.example.lab.utils.Database;
import com.example.lab.validation.ValidatorError;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderDescriptionValidation extends AbstractValidation implements Validation {

    private final String description;

    public OrderDescriptionValidation(String inputName, String locale, String email) {
        super(inputName, locale);
        this.description = email;
    }

    public void validate() throws SQLException {
        if (description == null || description.length() == 0) {
            this.errors.add(new ValidatorError("description", bundle.getString("description_required")));
        }
    }

    @Override
    public String oldValue() {
        return description;
    }

}
