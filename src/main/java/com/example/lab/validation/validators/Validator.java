package com.example.lab.validation.validators;


import com.example.lab.validation.ValidatorError;
import com.example.lab.validation.validations.Validation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Set;

public interface Validator {
    boolean hasErrors();

    Set<ValidatorError> getErrors();

    Set<ValidatorError> getErrorsByName(String name);

    boolean hasErrorsByName(String name);

    String getOldByName(String name);

    void addValidation(Validation validation);

    void validate();
}
