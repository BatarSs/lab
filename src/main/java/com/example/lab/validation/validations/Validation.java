package com.example.lab.validation.validations;
import com.example.lab.validation.ValidatorError;


import java.sql.SQLException;
import java.util.Set;

public interface Validation {

    Set<ValidatorError> getErrors();

    boolean isValid();

    void validate() throws SQLException;

    String oldValue();

    String getInputName();
}
