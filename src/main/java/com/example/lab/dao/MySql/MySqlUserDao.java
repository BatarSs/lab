package com.example.lab.dao.MySql;


import com.example.lab.dao.Dao;
import com.example.lab.dao.UserDao;
import com.example.lab.models.User;
import com.example.lab.utils.Database;
import com.example.lab.utils.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MySqlUserDao extends Dao<User> implements UserDao {

    private static MySqlUserDao instance;

    public synchronized static UserDao getInstance() {
        if (instance == null) {
            instance = new MySqlUserDao();
        }
        return instance;
    }

    @Override
    public User get(Connection connection, int id) throws SQLException {
        List<User> list = findByFields(connection,
                "SELECT * FROM users WHERE id = ?",
                id);

        if (list.isEmpty()) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public User getByEmail(Connection connection, String email) throws SQLException {
        List<User> list = findByFields(connection,
                "SELECT * FROM users WHERE email = ?",
                email);

        if (list.isEmpty()) {
            return null;
        }

        return list.get(0);

    }

    @Override
    public List<User> getAll(Connection connection) throws SQLException {
        return getAll(connection, "SELECT * FROM users ORDER BY id DESC");
    }

    @Override
    public List<User> getMastersByUser(Connection connection, User user) throws SQLException {
        return findByFields(connection, "SELECT DISTINCT u.* FROM users u JOIN orders o ON u.id=o.master_id WHERE user_id =?  ORDER BY u.id DESC", user.getId());

    }

    @Override
    public List<User> getMastersByMaster(Connection connection, User user) throws SQLException {
        return findByFields(connection, "SELECT * FROM users WHERE role = ? ORDER BY id DESC", Role.USER.toString());

    }

    @Override
    public List<User> getMastersByManager(Connection connection, User user) throws SQLException {
        return findByFields(connection, "SELECT * FROM users WHERE role = ? ORDER BY id DESC", Role.MASTER.toString());

    }

    @Override
    public int save(Connection connection, User user) throws SQLException {
        return save(connection,
                "INSERT INTO users (name, email, balance, password, avatar) VALUES (?, ?, ?, ?, ?)",
                user);

    }

    @Override
    public void update(Connection connection, User user) throws SQLException {
        super.update(connection,
                "UPDATE Users SET name=?, email=?, balance=?, password=?, avatar=? WHERE id = ?", user, 5, user.getId());

    }

    @Override
    public void delete(Connection connection, User user) throws SQLException {
        super.update(connection,
                "DELETE FROM users WHERE id = ?", user, 1, user.getId());

    }

    @Override
    protected void prepareStatementFromModel(PreparedStatement ps, User user) throws SQLException {
        ps.setString(1, user.getName());
        ps.setString(2, user.getEmail());
        ps.setInt(3, user.getBalance());
        ps.setString(4, user.getPassword());
        if (user.getAvatar() != null) {
            ps.setString(5, user.getAvatar());
        } else {
            ps.setString(5, "default.png");
        }
    }

    @Override
    protected User getModelFromResult(ResultSet rs) throws SQLException {
        User user = new User();

        user.setId(rs.getInt("id"));
        user.setName(rs.getString("name"));
        user.setEmail(rs.getString("email"));
        user.setPassword(rs.getString("password"));
        user.setBalance(rs.getInt("balance"));
        user.setRole(Role.valueOf(rs.getString("role")));
        user.setAvatar(rs.getString("avatar"));

        return user;
    }
}
