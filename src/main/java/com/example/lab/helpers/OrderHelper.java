package com.example.lab.helpers;

import com.example.lab.exception.DBException;
import com.example.lab.models.Order;
import com.example.lab.models.User;
import com.example.lab.services.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;

import java.sql.SQLException;
import java.util.List;

public class OrderHelper {

    public static int getOrdersCount(User user) throws DBException {
        OrderManager orderManager = new OrderManager(user);
        return orderManager.getOrdersCount();
    }

    public static List<Order> getOrders(User user) throws DBException {
        OrderManager orderManager = new OrderManager(user);
        return orderManager.getOrders();
    }
    public static Order getOrder(int orderId) throws SQLException {
        return ServiceFactory.getInstance().getOrderService().getById(orderId);
    }


    public static List<Order> getOrders(User user, int page, int itemsOnPage) throws SQLException {
        return ServiceFactory.getInstance().getOrderService().getByUserId(user.getId(), page, itemsOnPage);
    }

    public static String getLink(HttpServletRequest request, Integer page) {
        String qs = request.getQueryString();

        if (qs != null && qs.length() > 0) {
            if (qs.contains("page=")) {
                qs = qs.replaceFirst("page=[\\d]+", "page=" + page);
            } else {
                qs = qs + "&page=" + page;
            }
        } else {
            qs = "page=" + page;
        }

        return request.getRequestURL().append("?").append(qs).toString();
    }
}
