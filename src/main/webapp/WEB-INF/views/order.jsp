<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="orderTag" uri="http://www.my.tag/example" %>
<jsp:useBean id="reviewHelper" class="com.example.lab.helpers.ReviewHelper"/>
<jsp:useBean id="userHelper" class="com.example.lab.helpers.UserHelper"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>
<t:wrapper>
    <a href="${pageContext.request.contextPath}/p/orders"
       class="btn btn-primary ms-3 mt-4"><fmt:message key="back" /></a>
    <div class="row mt-3 mb-2 justify-content-center">
        <orderTag:OrderTag order="${order}" locale="${cookie['lang'].value}" user="${authUser}" showEdit="${true}" classes="col col-12 col-sm-8" />
    </div>
    <c:if test="${reviews.size() > 0}">
        <div class="row">
            <h2 class="text-center mb-3"><fmt:message key="reviews" /></h2>
            <c:forEach var="review" items="${reviews}">
                <div class="col-sm-6 col-md-4 col-lg-3 mb-4">
                    <div class="card review">
                        <div class="card-body">
                            <h5 class="card-title">${userHelper.getUserNameWithAvatar(order.getUserId())}</h5>
                            <h6 class="card-subtitle mb-2 text-muted"><fmt:message key="made" />: ${userHelper.getUserNameWithAvatar(order.getMasterId())}</h6>
                            <span class="review-mark">${reviewHelper.getMark(review)}</span>
                            <p class="card-text">${review.getComment()}</p>
                            <c:if test="${review.getComment().length() > 100}">
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#reviewModal">
                                    <fmt:message key="show_text" />
                                </button>
                            </c:if>
                        </div>
                    </div>
                </div>

            </c:forEach>
        </div>

        <div class="modal fade" id="reviewModal" tabindex="-1" aria-labelledby="reviewModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="reviewModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body review">
                        <h6 class="card-subtitle mb-2 text-muted"></h6>
                        <span class="review-mark"></span>
                        <p class="card-text"></p>
                    </div>
                </div>
            </div>
        </div>
    </c:if>
</t:wrapper>
