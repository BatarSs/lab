package com.example.lab.controllers;

import com.example.lab.exception.DBException;
import com.example.lab.filters.LoginFilter;
import com.example.lab.models.User;
import com.example.lab.services.ServiceFactory;
import com.example.lab.services.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.sql.SQLException;

/**
 * Controller for Login actions
 */
@ControllerAnnotation("/login")
public class AuthController implements Controller {

    private static final UserService userService;
    private static final Logger LOGGER = LogManager.getLogger(AuthController.class);

    static {
        userService = ServiceFactory.getInstance().getUserService();
    }

    /**
     * Return name of jsp for login page
     *
     * @param request HttpRequest contains the client's request
     * @param response HttpResponse contains the servlet's response
     *
     * @return jsp page name without extension
     */
    @Override
    public String get(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        if (session.getAttribute("errors") != null) {
            request.setAttribute("errors", session.getAttribute("errors"));
            session.removeAttribute("errors");
        }

        return "login";
    }

    /**
     * Authenticate user by email and login, validation in {@link LoginFilter}
     *
     * @param request HttpRequest contains the client's request
     * @param response HttpResponse contains the servlet's response
     *
     * @return name of page for redirect if login successfully
     *
     * @throws DBException
     *         If user not found by email or other {@link SQLException}
     */
    @Override
    public String post(HttpServletRequest request, HttpServletResponse response) throws DBException {
        User user;

        try {
            user = userService.getByEmail(request.getParameter("email"));
        } catch (SQLException exception) {
            LOGGER.error("User not found", exception);
            throw new DBException("User not found", exception);
        }

        HttpSession session = request.getSession();
        session.setAttribute("authUser", user);

        LOGGER.debug("User logged: " + user.getId());

        return "profile";
    }
}
