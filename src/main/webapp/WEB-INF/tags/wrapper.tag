<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag description="Wrapper Tag" pageEncoding="UTF-8" %>
<jsp:useBean id="userHelper" class="com.example.lab.helpers.UserHelper"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>

<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Repair lab</title>
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/noty.css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/resources/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/resources/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/resources/favicon-16x16.png">
    <link rel="manifest" href="${pageContext.request.contextPath}/resources/site.webmanifest">
    <link rel="mask-icon" href="${pageContext.request.contextPath}/resources/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

<%--    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"--%>
<%--          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">--%>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<%--    <link href=" https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet" type="text/css">--%>

<%--    <link href="${pageContext.request.contextPath}/resources/fa.css" rel="stylesheet">--%>
    <link href="${pageContext.request.contextPath}/resources/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/owl.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/datatables.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/app.css" rel="stylesheet" type="text/css">
</head>
<body class="d-flex flex-column h-100">

<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/p/">
                <img class="filter-invert" src="${pageContext.request.contextPath}/resources/images/logo.png" alt="" width="62" height="49">
                Repair Lab
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="${pageContext.request.contextPath}/p/"><fmt:message key="home" /></a>
                    </li>
                    <c:if test="${authUser != null}">
                        <li class="nav-item">
                            <a class="nav-link" href="${pageContext.request.contextPath}/p/orders"><fmt:message key="orders" /></a>
                        </li>
                        <c:if test="${authUser.isManager()}">
                            <li class="nav-item">
                                <a class="nav-link" href="${pageContext.request.contextPath}/p/users"><fmt:message key="users" /></a>
                            </li>
                        </c:if>
                    </c:if>
                </ul>
                <div class="d-flex mb-0">
                    <c:choose>
                        <c:when test="${authUser != null}">
                            <div class="header-name">
                                ${userHelper.getUserNameWithAvatar(authUser.getId())}
                            </div>
                            <a class="btn btn-primary" href="${pageContext.request.contextPath}/p/profile"><fmt:message key="profile" /></a>
                        </c:when>
                        <c:otherwise>
                            <a class="btn btn-primary me-2" href="${pageContext.request.contextPath}/p/login"><fmt:message key="login" /></a>
                            <a class="btn btn-warning" href="${pageContext.request.contextPath}/p/register"><fmt:message key="register" /></a>
                        </c:otherwise>
                    </c:choose>
                    <div class="flex-shrink-0 dropdown d-flex mx-3">
                        <a href="#" class="link-light text-decoration-none dropdown-toggle d-flex align-items-center" id="dropdownUser2" data-bs-toggle="dropdown" aria-expanded="false">
                            <span class="text-uppercase">${cookie['lang'].value}</span>
                        </a>
                        <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2" style="width: 52px;min-width: auto;">
                            <li><a class="dropdown-item" href="${requestScope['javax.servlet.forward.request_uri']}?locale=en">EN</a></li>
                            <li><a class="dropdown-item" href="${requestScope['javax.servlet.forward.request_uri']}?locale=ru">RU</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>

<!-- Begin page content -->
<main>
    <div class="container">
        <jsp:doBody/>
    </div>
</main>

<footer class="footer mt-auto py-3 bg-light">
    <div class="container">
        <span class="text-muted">Repair service 2022</span>
    </div>
</footer>

<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>--%>
<%--<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"--%>
<%--        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"--%>
<%--        crossorigin="anonymous"></script>--%>
<script src="${pageContext.request.contextPath}/resources/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap.js"></script>
<%--<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.4/datatables.min.js"></script>--%>
<script src="${pageContext.request.contextPath}/resources/datatables.js"></script>
<script src="${pageContext.request.contextPath}/resources/noty.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/owl.js"></script>
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>--%>
<c:if test="${notification != null}">
    <script>
        setTimeout(function (){
            new Noty({
                type: '${notification.getType()}',
                theme    : 'mint',
                text: '${notification.getText()}',
                timeout: 2000
            }).show();
        }, 100)
    </script>
</c:if>

<script src="${pageContext.request.contextPath}/resources/scripts.js"></script>

</body>
</html>
