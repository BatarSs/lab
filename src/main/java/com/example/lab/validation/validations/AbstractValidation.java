package com.example.lab.validation.validations;

import com.example.lab.validation.ValidatorError;

import java.util.HashSet;
import java.util.Locale;
import java.util.ResourceBundle;

public abstract class AbstractValidation implements Validation {

    protected final HashSet<ValidatorError> errors = new HashSet<>();
    protected final String inputName;
    protected final ResourceBundle bundle;

    protected AbstractValidation(String inputName, String locale) {
        this.inputName = inputName;
        this.bundle = ResourceBundle.getBundle("messages", Locale.forLanguageTag(locale));
    }

    @Override
    public boolean isValid() {
        return this.errors.size() == 0;
    }

    @Override
    public HashSet<ValidatorError> getErrors() {
        return this.errors;
    }

    public String getInputName() {
        return inputName;
    }
}
