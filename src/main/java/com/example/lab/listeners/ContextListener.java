package com.example.lab.listeners;

import com.example.lab.utils.Cache;
import com.example.lab.utils.Context;
import com.example.lab.utils.MemoryCache;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        Cache cache = new MemoryCache();
        context.setAttribute("cache", cache);
        Context.setContext(sce.getServletContext());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        Cache cache = (Cache) context.getAttribute("cache");
        cache.clear();
        context.removeAttribute("cache");
        Context.destroyContext();
    }
}
