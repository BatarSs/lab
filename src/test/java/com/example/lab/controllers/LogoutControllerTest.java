package com.example.lab.controllers;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class LogoutControllerTest {

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession session;


    @Test
    public void logoutTest() throws SQLException {
        String expectedPage = "login";

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);

        when(request.getSession()).thenReturn(session);

        String page = new LogoutController().post(request, response);

        assertEquals(expectedPage, page);
        verify(session, times(1)).invalidate();
    }
}
