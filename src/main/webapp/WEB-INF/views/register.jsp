<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>

<t:wrapper>
    <div class="form-register">
        <form method="post" action="${pageContext.request.contextPath}/p/register">
            <img class="mb-4" src="${pageContext.request.contextPath}/resources/images/logo.png" alt="" width="72" height="57">
            <h1 class="h3 mb-3 fw-normal"><fmt:message key="register" /></h1>
            <c:if test="${errors.hasErrorsByName('email')}">
                <c:forEach var="error" items="${errors.getErrorsByName('email')}">
                    <p><c:out value="${error.getDescription()}" /></p>
                </c:forEach>
            </c:if>
            <c:choose>
                <c:when test="${errors.hasErrors()}">
                    <div class="form-floating mb-3">
                        <input type="email" name="email" value="${errors.getOldByName('email')}" class="form-control" id="floatingInput" placeholder="name@example.com">
                        <label for="floatingInput"><fmt:message key="email" /></label>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-floating mb-3">
                        <input type="email" name="email" class="form-control" id="floatingInput1" placeholder="name@example.com">
                        <label for="floatingInput1"><fmt:message key="email" /></label>
                    </div>
                </c:otherwise>
            </c:choose>

            <c:if test="${errors.hasErrorsByName('name')}">
                <c:forEach var="error" items="${errors.getErrorsByName('name')}">
                    <p><c:out value="${error.getDescription()}" /></p>
                </c:forEach>
            </c:if>
            <c:choose>
                <c:when test="${errors.hasErrors()}">
                    <div class="form-floating mb-3">
                        <input type="text" name="name" value="${errors.getOldByName('name')}" class="form-control" id="floatingInputName" placeholder="Ivan">
                        <label for="floatingInputName"><fmt:message key="name" /></label>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-floating mb-3">
                        <input type="text" name="name" class="form-control" id="floatingInputName1" placeholder="Ivan">
                        <label for="floatingInputName1"><fmt:message key="name" /></label>
                    </div>
                </c:otherwise>
            </c:choose>

            <c:if test="${errors.hasErrorsByName('password')}">
                <c:forEach var="error" items="${errors.getErrorsByName('password')}">
                    <p><c:out value="${error.getDescription()}" /></p>
                </c:forEach>
            </c:if>
            <div class="form-floating mb-3">
                <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="<fmt:message key="password" />">
                <label for="floatingPassword"><fmt:message key="password" /></label>
            </div>
            <div class="form-floating mb-3">
                <input type="password" name="password_confirm" class="form-control" id="floatingPasswordRepeat" placeholder="<fmt:message key="confirm_password" />">
                <label for="floatingPasswordRepeat"><fmt:message key="users" /></label>
            </div>
            <div class="mb-3 form-check center">
                <input type="checkbox" class="form-check-input" required id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1"><fmt:message key="accept" /></label>
            </div>
            <button type="submit" class="w-100 btn btn-lg btn-primary"><fmt:message key="submit" /></button>
        </form>
    </div>
</t:wrapper>
