package com.example.lab.controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

@WebServlet("/images/*")
public class ImagesServlet extends HttpServlet {

    private final static int CACHE_TIME_IN_SEC = 1000000;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        File uploads = new File(request.getServletContext().getInitParameter("upload.location"));
        String fileName = request.getPathInfo();
        File image = new File(uploads + fileName);

        if (image.exists() && image.isFile()) {
            OutputStream out = response.getOutputStream();
            response.setContentType(Files.probeContentType(Paths.get(image.getPath())));
            FileInputStream in = new FileInputStream(image);

            long expiry = new Date().getTime() + CACHE_TIME_IN_SEC * 1000;
            response.setDateHeader("Expires", expiry);
            response.setHeader("Cache-Control", "max-age=" + CACHE_TIME_IN_SEC);
            response.setContentLength((int) image.length());

            byte[] buf = new byte[1024];
            int count = 0;
            while ((count = in.read(buf)) >= 0) {
                out.write(buf, 0, count);
            }
        }

        getServletContext().getRequestDispatcher("/WEB-INF/views/404.jsp").forward(request, response);
    }
}
