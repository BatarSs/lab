<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="orderHelper" class="com.example.lab.helpers.OrderHelper"/>
<jsp:useBean id="userHelper" class="com.example.lab.helpers.UserHelper"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>

<t:wrapper>
    <div class="main-body">
        <div class="row gutters-sm mt-4">
            <div class="col-md-4 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="${pageContext.request.contextPath}/images/${authUser.getAvatar()}" alt="${user.getName()}" class="rounded-circle" width="150">
                                <div class="mt-3">
                                    <h4>${authUser.getName()}</h4>
                                    <p class="text-secondary mb-1">${userHelper.getRoleName(authUser)}</p>
                                        <%--                                    <p class="text-muted font-size-sm">Bay Area, San Francisco, CA</p>--%>
                                        <%--                                    <button class="btn btn-primary">Follow</button>--%>
                                    <c:if test="${user.isUser()}">
                                        <a class="btn btn-info mt-4" href="${pageContext.request.contextPath}/p/orders/create">Create order</a>
                                    </c:if>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-3">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <a href="${pageContext.request.contextPath}/p/orders">
                                <h6 class="mb-0"><fmt:message key="orders" /></h6>
                            </a>
                            <span class="text-secondary">${orderHelper.getOrdersCount(authUser)}</span>
                        </li>
                        <c:if test="${authUser.isManager()}">
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <a href="${pageContext.request.contextPath}/p/users">
                                    <h6 class="mb-0"><fmt:message key="users" /></h6>
                                </a>
                                <span class="text-secondary">${userHelper.getUsersCount()}</span>
                            </li>
                        </c:if>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <form class="card" method="post" action="${pageContext.request.contextPath}/p/profile/settings" enctype="multipart/form-data">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <h6 class="mb-0"><fmt:message key="name" /></h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="text" name="name" class="form-control" value="${authUser.getName()}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <h6 class="mb-0"><fmt:message key="email" /></h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="email" name="email" class="form-control" value="${authUser.getEmail()}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <h6 class="mb-0"><fmt:message key="avatar" /></h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="file" name="image" class="form-control" accept=".jpg, .jpeg, .png" value="">
                            </div>
                        </div>
                        <c:if test="${authUser.isUser()}">
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-0"><fmt:message key="balance" /></h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="number" name="balance" class="form-control" value="${authUser.getBalance()}">
                                </div>
                            </div>
                        </c:if>
<%--                        <div class="row mb-3">--%>
<%--                            <div class="col-sm-3">--%>
<%--                                <h6 class="mb-0">Phone</h6>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-9 text-secondary">--%>
<%--                                <input type="text" class="form-control" value="(239) 816-9029">--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="row mb-3">--%>
<%--                            <div class="col-sm-3">--%>
<%--                                <h6 class="mb-0">Mobile</h6>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-9 text-secondary">--%>
<%--                                <input type="text" class="form-control" value="(320) 380-4539">--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="row mb-3">--%>
<%--                            <div class="col-sm-3">--%>
<%--                                <h6 class="mb-0">Address</h6>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-9 text-secondary">--%>
<%--                                <input type="text" class="form-control" value="Bay Area, San Francisco, CA">--%>
<%--                            </div>--%>
<%--                        </div>--%>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="${pageContext.request.contextPath}/p/profile" class="btn btn-warning px-4"><fmt:message key="back" /></a>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="submit" class="btn btn-primary px-4" value="<fmt:message key="save" />">
                            </div>
                        </div>
                    </div>
                </form>



            </div>
        </div>

    </div>
</t:wrapper>
