package com.example.lab.controllers;

import com.example.lab.exception.DBException;
import com.example.lab.helpers.UserManager;
import com.example.lab.models.User;
import com.example.lab.utils.Role;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.sql.SQLException;
import java.util.List;


@ControllerAnnotation("/users")
public class UsersController extends BaseController implements Controller {

    @Override
    @AuthAnnotation(roles = {Role.MANAGER})
    public String get(HttpServletRequest request, HttpServletResponse response) throws DBException {
        UserManager userManager = new UserManager();

        List<User> users = userManager.getUsers();

        request.setAttribute("users", users);

        return "users";
    }
}
