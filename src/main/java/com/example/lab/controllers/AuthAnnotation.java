package com.example.lab.controllers;

import com.example.lab.utils.Role;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that the method requires authentication or certain privileges
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthAnnotation {
    /**
     * Return the array of {@link Role} who have access to method if empty
     * array it's mean all roles have access
     * @return Return the array of {@link Role}
     */
    Role[] roles() default {};
}
