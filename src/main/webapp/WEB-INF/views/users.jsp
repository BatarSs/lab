<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="orderHelper" class="com.example.lab.helpers.OrderHelper"/>
<%@ taglib prefix="orderTag" uri="http://www.my.tag/example" %>
<%@ page import="com.example.lab.utils.Role" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>

<t:wrapper>
    <h2 class="mb-3 mt-2"><fmt:message key="users" /></h2>
    <div class="d-flex row mx-1 filter-form align-items-center">
        <div class="col-6 col-md-3 my-2 d-flex  align-items-center">
            <input type="radio" checked class="me-2 users-filter" value="ALL" id="all-select" name="users" />
            <label for="all-select" class="form-label mb-0"><fmt:message key="all" /></label>
        </div>

        <div class="col-6 col-md-3 my-2 d-flex  align-items-center">
            <input type="radio" class="me-2 users-filter" value="${Role.MASTER.getName(cookie['lang'].value)}" id="master-select" name="users" />
            <label for="master-select" class="form-label mb-0"><fmt:message key="masters" /></label>
        </div>

        <div class="col-6 col-md-3 my-2 d-flex align-items-center">
            <input type="radio" class="me-2 users-filter" value="${Role.USER.getName(cookie['lang'].value)}" id="users-select" name="users" />
            <label for="users-select" class="form-label mb-0"><fmt:message key="users" /></label>
        </div>

        <div class="col-6 col-md-3 my-2 d-flex align-items-center">
            <input type="radio" class="me-2 users-filter" value="${Role.MANAGER.getName(cookie['lang'].value)}" id="manager-select" name="users" />
            <label for="manager-select" class="form-label mb-0"><fmt:message key="managers" /></label>
        </div>
    </div>
    <table id="users-table" class="display" style="width:100%">
        <thead class="">
        <tr>
            <th>ID</th>
            <th><fmt:message key="name" /></th>
            <th><fmt:message key="role" /></th>
            <th><fmt:message key="orders_count" /></th>
            <th><fmt:message key="actions" /></th>
                <%--                <th>Age</th>--%>
                <%--                <th>Start date</th>--%>
                <%--                <th>Salary</th>--%>
        </tr>
        </thead>
        <tbody>

            <c:forEach var="user" items="${users}">
            <tr>
                <td class="ps-3">${user.getId()}</td>
                <td>${user.getName()}</td>
                <td>${user.getRole().getName(cookie['lang'].value)}</td>
                <td>
                    <c:if test="${user.isUser() || user.isMaster()}">
                        ${orderHelper.getOrdersCount(user)}
                    </c:if>
                </td>
                <td>
                    <a href="${pageContext.request.contextPath}/p/users/${user.getId()}" class="my-2 btn btn-warning"><fmt:message key="view" /></a>
                </td>
            </tr>
            </c:forEach>

        </tbody>
    </table>
<%--    <div class="row my-5">--%>
<%--        <c:forEach var="user" items="${users}">--%>
<%--            <div class="col col-12 col-sm-6 col-lg-4 col-xl-3">--%>
<%--                <div class="card mx-3 mb-3">--%>
<%--                    <div class="card-body">--%>
<%--                        <h5 class="card-title">User #${user.getId()}</h5>--%>
<%--                        <p class="card-text">${user.getRole().getName()} </p>--%>
<%--                    </div>--%>
<%--                    <ul class="list-group list-group-flush">--%>
<%--                        <li class="list-group-item">Email: ${user.getEmail()}</li>--%>
<%--                        <li class="list-group-item">Balance: ${user.getBalance()}</li>--%>
<%--                        <c:if test="${user.isMaster() || user.isUser()}">--%>
<%--                            <li class="list-group-item">Orders count: ${orderHelper.getOrdersCount(user)}</li>--%>
<%--                        </c:if>--%>
<%--                    </ul>--%>
<%--                    <div class="card-body d-flex flex-row-reverse gap-2">--%>
<%--                        <a href="${pageContext.request.contextPath}/p/users/${user.getId()}" class="btn btn-warning">View</a>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </c:forEach>--%>
<%--    </div>--%>
<%--    <c:if test="${pages > 0}">--%>
<%--        <nav aria-label="Page navigation example" class="d-flex justify-content-center">--%>
<%--            <ul class="pagination">--%>
<%--                <c:forEach var="i" begin="1" end="${pages}">--%>
<%--                    <c:choose>--%>
<%--                        <c:when test="${page == i}">--%>
<%--                            <li class="page-item active" aria-current="page">--%>
<%--                                <a class="page-link" href="#"><c:out value="${i}"/></a>--%>
<%--                            </li>--%>
<%--                        </c:when>--%>
<%--                        <c:otherwise>--%>
<%--                            <li class="page-item">--%>
<%--                                <a class="page-link"--%>
<%--                                   href="${orderHelper.getLink(request, i)}"><c:out--%>
<%--                                        value="${i}"/></a>--%>
<%--                            </li>--%>
<%--                        </c:otherwise>--%>
<%--                    </c:choose>--%>
<%--                </c:forEach>--%>
<%--            </ul>--%>
<%--        </nav>--%>
<%--    </c:if>--%>
</t:wrapper>
