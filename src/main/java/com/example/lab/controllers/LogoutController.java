package com.example.lab.controllers;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.sql.SQLException;

@ControllerAnnotation("/profile/logout")
public class LogoutController implements Controller {

    @AuthAnnotation
    @Override
    public String post(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        session.invalidate();

        return "login";
    }
}
