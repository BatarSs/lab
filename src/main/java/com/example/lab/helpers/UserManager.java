package com.example.lab.helpers;

import com.example.lab.dao.DaoFactory;
import com.example.lab.dao.UserDao;
import com.example.lab.exception.DBException;
import com.example.lab.models.User;
import com.example.lab.services.ServiceFactory;
import com.example.lab.services.UserService;

import java.sql.SQLException;
import java.util.List;

public class UserManager {

    private static final UserService userService;

    static {
        userService = ServiceFactory.getInstance().getUserService();
    }

    public List<User> getUsers() throws DBException {
        try {
            return userService.getAll();
        } catch (SQLException exception) {
            throw new DBException("Users not found", exception);
        }
    }

    public User getUserById(int id) throws DBException {
        try {
            return userService.get(id);
        } catch (SQLException exception) {
            throw new DBException("User not found", exception);
        }
    }
}
