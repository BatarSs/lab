package com.example.lab.helpers;

import com.example.lab.dao.DaoFactory;
import com.example.lab.exception.DBException;
import com.example.lab.models.Order;
import com.example.lab.models.Review;
import com.example.lab.models.User;
import com.example.lab.services.ReviewService;
import com.example.lab.services.ServiceFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReviewManager {

    private static final ReviewService reviewService;

    static {
        reviewService = ServiceFactory.getInstance().getReviewService();
    }

    public List<Review> getReviewsByOrder(Order order) throws DBException {
        try {
            return reviewService.getByOrderId(order.getId());
        } catch (SQLException exception) {
            throw new DBException("Reviews not found", exception);
        }
    }

    public List<Review> getReviewsByUser(User user) throws DBException {
        try {
            if(user.isUser()) {
                return reviewService.getByUserId(user.getId());
            } else if(user.isMaster()) {
                return reviewService.getByMasterId(user.getId());
            }
        } catch (SQLException exception) {
            throw new DBException("Reviews not found", exception);
        }

        return new ArrayList<>();
    }

    public List<Review> getLastReviews(int count) throws DBException {
        try {
            return reviewService.getAll(1, count);
        } catch (SQLException exception) {
            throw new DBException("Reviews not found", exception);
        }
    }
}
