package com.example.lab.validation.validations;

import com.example.lab.validation.ValidatorError;

public class SamePasswordValidation extends AbstractValidation implements Validation {

    private final String password;
    private final String confirmPassword;

    public SamePasswordValidation(String inputName, String locale, String password, String confirmPassword) {
        super(inputName, locale);
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    @Override
    public void validate() {
        if(!password.equals(confirmPassword)) {
            this.errors.add(new ValidatorError("password", bundle.getString("diff_password")));
        }
    }

    @Override
    public String oldValue() {
        return password;
    }
}
