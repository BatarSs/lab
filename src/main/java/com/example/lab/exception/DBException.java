package com.example.lab.exception;

/**
 * Thrown when a method catch {@link java.sql.SQLException} or
 * get empty result of database query
 */
public class DBException extends Exception {
    /**
     * Constructs a new exception with the specified detail message concerning
     * database query (which query failed or not found result) and cause.
     *
     * @param  message the detail message (which is saved for later retrieval
     *         by the {@link Throwable#getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link Throwable#getCause()} method).  (A {@code null} value
     *         is permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     */
    public DBException(String message, Throwable cause) {
        super(message, cause);
    }
}
