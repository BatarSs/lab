package com.example.lab.services;

import com.example.lab.models.User;

import java.sql.SQLException;
import java.util.List;

public interface UserService {
    User get(int id) throws SQLException;

    User getByEmail(String email) throws SQLException;

    List<User> getAll() throws SQLException;

    List<User> getMastersByUser(User user) throws SQLException;

    List<User> getMastersByMaster(User user) throws SQLException;

    List<User> getMastersByManager(User user) throws SQLException;

    int save(User user) throws SQLException;

    void update(User user) throws SQLException;

    void delete(User user) throws SQLException;
}
