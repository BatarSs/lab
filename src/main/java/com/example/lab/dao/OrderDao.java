package com.example.lab.dao;

import com.example.lab.models.Order;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface OrderDao {

    Order getByEmail(Connection connection, String email) throws SQLException;

    Order getById(Connection connection, int id) throws SQLException;

    List<Order> getAll(Connection connection) throws SQLException;

    int getAllCount(Connection connection) throws SQLException;

    List<Order> getAll(Connection connection, int page, int itemsOnPage) throws SQLException;

    List<Order> getByUserId(Connection connection, Integer id) throws SQLException;

    List<Order> getByUserId(Connection connection, Integer id, int page, int itemsOnPage) throws SQLException;

    List<Order> getByMasterId(Connection connection, Integer id) throws SQLException;

    List<Order> getByMasterId(Connection connection, Integer id, int page, int itemsOnPage) throws SQLException;

    int getByMasterIdCount(Connection connection, Integer id) throws SQLException;

    int getByUserIdCount(Connection connection, Integer id) throws SQLException;

    int save(Connection connection, Order order) throws SQLException;

    void update(Connection connection, Order order) throws SQLException;

    void delete(Connection connection, Order order) throws SQLException;
}
