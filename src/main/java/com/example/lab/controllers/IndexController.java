package com.example.lab.controllers;

import com.example.lab.exception.DBException;
import com.example.lab.helpers.ReviewManager;
import com.example.lab.dao.DaoFactory;
import com.example.lab.models.Review;
import com.example.lab.models.User;
import com.example.lab.services.ServiceFactory;
import com.example.lab.utils.Cache;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;

@ControllerAnnotation("/")
public class IndexController extends BaseController implements Controller {

    private final static int CACHE_TIME_IN_SECS = 10;
    private final static int LAST_REVIEWS_COUNT = 10;
    private static final Logger LOGGER = LogManager.getLogger(IndexController.class);

    @Override
    public String get(HttpServletRequest request, HttpServletResponse response) throws DBException {
        Cache cache = (Cache) request.getServletContext().getAttribute("cache");

        if(cache.has("reviews")) {
            request.setAttribute("reviews", cache.get("reviews"));
        } else {
            ReviewManager reviewManager = new ReviewManager();
            List<Review> reviews = reviewManager.getLastReviews(LAST_REVIEWS_COUNT);
            request.setAttribute("reviews", reviews);
            cache.add("reviews", reviews, CACHE_TIME_IN_SECS);
        }

        if(cache.has("ordersCount")) {
            request.setAttribute("ordersCount", cache.get("ordersCount"));
        } else {
            int ordersCount = 0;
            try {
                ordersCount = ServiceFactory.getInstance().getOrderService().getAll().size();
            } catch (SQLException exception) {
                LOGGER.error("Orders not found", exception);
                throw new DBException("Orders not found", exception);
            }
            request.setAttribute("ordersCount", ordersCount);
            cache.add("ordersCount", ordersCount, CACHE_TIME_IN_SECS);
        }

        if(cache.has("usersCount")) {
            request.setAttribute("usersCount", cache.get("usersCount"));
        } else {
            List<User> users = null;
            try {
                users = ServiceFactory.getInstance().getUserService().getAll();
            } catch (SQLException exception) {
                LOGGER.error("Users not found", exception);
                throw new DBException("Users not found", exception);
            }
            long usersCount = users.stream().filter(User::isUser).count();
            request.setAttribute("usersCount", usersCount);
            cache.add("usersCount", usersCount, CACHE_TIME_IN_SECS);
        }

        if(cache.has("mastersCount")) {
            request.setAttribute("mastersCount", cache.get("mastersCount"));
        } else {
            List<User> users = null;
            try {
                users = ServiceFactory.getInstance().getUserService().getAll();
            } catch (SQLException exception) {
                LOGGER.error("Masters not found", exception);
                throw new DBException("Masters not found", exception);
            }
            long mastersCount = users.stream().filter(User::isMaster).count();
            request.setAttribute("mastersCount", mastersCount);
            cache.add("mastersCount", mastersCount, CACHE_TIME_IN_SECS);
        }

        return "index";
    }
}
