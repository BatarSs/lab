package com.example.lab.controllers;


import com.example.lab.exception.DBException;
import com.example.lab.helpers.OrderManager;
import com.example.lab.models.Order;
import com.example.lab.models.User;
import com.example.lab.services.OrderService;
import com.example.lab.services.ServiceFactory;
import com.example.lab.utils.Notification;
import com.example.lab.utils.OrderStatus;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;

@ControllerAnnotation("/orders/*/edit")
public class OrderEditController extends BaseController implements Controller {

    private static final OrderService orderService;
    private static final Logger LOGGER = LogManager.getLogger(OrderEditController.class);

    static {
        orderService = ServiceFactory.getInstance().getOrderService();
    }

    @Override
    @AuthAnnotation
    public String get(HttpServletRequest request, HttpServletResponse response) throws DBException {
        User user = (User) request.getAttribute("authUser");

        Integer[] ids = getIdsFromRequest(request, 1);

        OrderManager orderManager = new OrderManager(user);
        Order order = orderManager.getOrder(ids[0]);

        List<User> masters = orderManager.getMasters();

        request.setAttribute("masters", masters);
        request.setAttribute("statuses", OrderStatus.values());
        request.setAttribute("order", order);

        return "order-edit";
    }

    @Override
    @AuthAnnotation
    public String post(HttpServletRequest request, HttpServletResponse response) throws DBException {
        User user = (User) request.getAttribute("authUser");

        Integer[] ids = getIdsFromRequest(request, 1);
        OrderManager orderManager = new OrderManager(user);
        Order order = orderManager.getOrder(ids[0]);

        if(request.getParameter("description") != null) {
            order.setDescription(request.getParameter("description"));
        }

        if(request.getParameter("status_id") != null) {
            order.setStatus(OrderStatus.get(Integer.parseInt(request.getParameter("status_id"))));
        }

        if(request.getParameter("price") != null) {
            order.setPrice(Integer.parseInt(request.getParameter("price")));
        }

        if(request.getParameter("master_id") != null) {
            order.setMasterId(Integer.parseInt(request.getParameter("master_id")));
        }

        try {
            orderService.update(order);
        } catch (SQLException exception) {
            LOGGER.error("Order not updated",exception);
            throw new DBException("Order not updated", exception);
        }

        HttpSession session = request.getSession();
        session.setAttribute("notification", new Notification(Notification.Type.SUCCESS, "Order updated"));

        return "orders/" + order.getId() + "/edit";
    }
}
