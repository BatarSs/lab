package com.example.lab.controllers;

import com.example.lab.exception.DBException;
import com.example.lab.helpers.OrderManager;
import com.example.lab.dao.DaoFactory;
import com.example.lab.dao.ReviewDao;
import com.example.lab.models.Order;
import com.example.lab.models.Review;
import com.example.lab.models.User;
import com.example.lab.services.ReviewService;
import com.example.lab.services.ServiceFactory;
import com.example.lab.utils.Notification;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.sql.SQLException;

@ControllerAnnotation("/order/reviews/*")
public class ReviewsController extends BaseController implements Controller {

    @Override
    public String get(HttpServletRequest request, HttpServletResponse response) throws DBException {
        User user = (User) request.getAttribute("authUser");

        Integer[] ids = getIdsFromRequest(request, 1);
        OrderManager orderManager = new OrderManager(user);
        Order order = orderManager.getOrder(ids[0]);
        request.setAttribute("order", order);

        return "review-create";
    }

    @Override
    public String post(HttpServletRequest request, HttpServletResponse response) throws DBException {
        User user = (User) request.getAttribute("authUser");

        Integer[] ids = getIdsFromRequest(request, 1);
        OrderManager orderManager = new OrderManager(user);
        Order order = orderManager.getOrder(ids[0]);

        Review review = new Review();

        review.setOderId(order.getId());
        review.setStars(Integer.parseInt(request.getParameter("stars")));
        review.setComment(request.getParameter("comment"));

        try {
            ServiceFactory.getInstance().getReviewService().save(review);
        } catch (SQLException exception) {
            throw new DBException("Review not created", exception);
        }

        HttpSession session = request.getSession();
        session.setAttribute("notification", new Notification(Notification.Type.SUCCESS, "Review created"));

        return "orders/" + order.getId();
    }
}
