package com.example.lab.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Dao<T> {

    protected List<T> getAll(Connection connection, String sqlQuery) throws SQLException {
        return findByFields(connection, sqlQuery);
    }

    protected int save(Connection connection, String sqlQuery, T model) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            prepareStatementFromModel(ps, model);
            if (ps.executeUpdate() > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next())
                    return rs.getInt(1);
            }

        } finally {
            closeStatementsAndResultSet(ps, rs);
        }

        throw new SQLException("Not added");
    }

    protected List<T> findByFields(Connection connection, String sql, Object... values) throws SQLException {
        List<T> list = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement(sql);
            prepareQuery(ps, values);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(getModelFromResult(rs));
            }
        } finally {
            closeStatementsAndResultSet(ps, rs);
        }
        return list;
    }

    protected int countByFields(Connection connection, String sql, Object... values) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int result = 0;

        try {
            ps = connection.prepareStatement(sql);
            prepareQuery(ps, values);
            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("count");
            }
        } finally {
            closeStatementsAndResultSet(ps, rs);
        }

        return result;
    }

    protected <V> void update(Connection connection, String sql, T item, int rowCounts, V id)
            throws SQLException {
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement(sql);
            prepareStatementFromModel(ps, item);
            switch (id.getClass().getSimpleName()) {
                case "Integer":
                    ps.setInt(++rowCounts, (Integer) id);
                    break;
                case "String":
                    ps.setString(++rowCounts, (String) id);
                    break;
                default:
                    throw new SQLException("Invalid params");
            }
            if (ps.executeUpdate() == 0)
                throw new SQLException("Not updated");
        } finally {
            closeStatementsAndResultSet(ps, null);
        }
    }

    private void closeStatementsAndResultSet(PreparedStatement ps, ResultSet rs) {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    protected void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void prepareQuery(PreparedStatement ps, Object... values) throws SQLException {
        for (int i = 1; i <= values.length; i++) {
            Object value = values[i - 1];
            switch (value.getClass().getSimpleName()) {
                case "Integer":
                    ps.setInt(i, (Integer) value);
                    break;
                case "String":
                    ps.setString(i, (String) value);
                    break;
                default:
                    throw new SQLException("Invalid params");
            }
        }
    }

    protected abstract T getModelFromResult(ResultSet rs) throws SQLException;

    protected abstract void prepareStatementFromModel(PreparedStatement ps, T obj) throws SQLException;
}

