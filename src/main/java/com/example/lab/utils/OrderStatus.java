package com.example.lab.utils;

import java.util.Locale;
import java.util.ResourceBundle;

public enum OrderStatus {
    NEW, WAIT, CANCEL, WAITPAID, PAID, WORK, DONE, OVERDUE;

    public String getName() {
        return super.toString();
    }

    public String getName(String locale) {
        ResourceBundle bundle = ResourceBundle.getBundle("pages", Locale.forLanguageTag(locale));
        return bundle.getString(super.toString());
    }

    public static OrderStatus get(int ordinal) {
        return values()[ordinal];
    }
}
