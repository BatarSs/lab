package com.example.lab.utils;

import com.example.lab.helpers.ManagerStrategy;
import com.example.lab.helpers.MasterStrategy;
import com.example.lab.helpers.RoleStrategy;
import com.example.lab.helpers.UserStrategy;

import java.util.Locale;
import java.util.ResourceBundle;

public enum Role {
    USER(new UserStrategy()),
    MANAGER(new ManagerStrategy()),
    MASTER(new MasterStrategy());

    private final RoleStrategy strategy;

    public String getName(String locale) {
        ResourceBundle bundle = ResourceBundle.getBundle("pages", Locale.forLanguageTag(locale));
        return bundle.getString(super.toString());
    }

    Role(RoleStrategy roleStrategy) {
        this.strategy = roleStrategy;
    }

    public RoleStrategy getStrategy() {
        return strategy;
    }
}
