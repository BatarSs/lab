package com.example.lab.validation.validations;


import com.example.lab.utils.Database;
import com.example.lab.validation.ValidatorError;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NewUserEmailValidation extends AbstractValidation implements Validation {

    private final String email;

    public NewUserEmailValidation(String inputName, String locale, String email) {
        super(inputName, locale);
        this.email = email;
    }

    public void validate() throws SQLException {

//        Connection connection = Database.INSTANCE.openConnection();
//
//        assert connection != null;
//        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM users WHERE email = ?");
//        stmt.setString(1, this.email);
//        ResultSet resultSet = stmt.executeQuery();
//        connection.close();
//
//        if (resultSet.next()) {
//            this.errors.add(new ValidatorError("email", bundle.getString("user_email_not_found")));
//        }

    }

    @Override
    public String oldValue() {
        return email;
    }

}
