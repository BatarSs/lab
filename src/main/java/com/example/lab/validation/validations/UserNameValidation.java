package com.example.lab.validation.validations;

import com.example.lab.validation.ValidatorError;

public class UserNameValidation extends AbstractValidation implements Validation {

    private final String name;

    public UserNameValidation(String inputName, String locale, String name) {
        super(inputName, locale);
        this.name = name;
    }

    @Override
    public void validate() {
        if (name == null || name.length() == 0) {
            this.errors.add(new ValidatorError("name", bundle.getString("name_required")));
        }
    }

    @Override
    public String oldValue() {
        return name;
    }
}
