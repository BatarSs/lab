package com.example.lab.helpers;

import com.example.lab.models.User;

public abstract class AbstractManager {

    protected RoleStrategy role;
    protected User user;

    protected AbstractManager(User user) {
        this.user = user;
        this.role = user.getRole().getStrategy();
    }
}
