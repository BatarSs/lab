package com.example.lab.validation.validators;

import com.example.lab.validation.ValidatorError;
import com.example.lab.validation.validations.Validation;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.stream.Collectors;

public class BaseValidator implements Validator {

    protected final HashSet<ValidatorError> errors = new HashSet<>();
    private final HashSet<Validation> validations = new HashSet<>();

    @Override
    public boolean hasErrors() {
        return this.errors.size() > 0;
    }

    @Override
    public HashSet<ValidatorError> getErrors() {
        return this.errors;
    }

    @Override
    public HashSet<ValidatorError> getErrorsByName(String name) {
        return this.errors.stream()
                .filter(e -> e.getName().equals(name)).collect(Collectors.toCollection(HashSet::new));
    }

    @Override
    public boolean hasErrorsByName(String name) {
        return this.validations.stream().anyMatch(e -> e.getInputName().equals(name) && !e.isValid());
    }

    @Override
    public String getOldByName(String name) {
        if (this.validations.stream().anyMatch(e -> e.getInputName().equals(name))) {
            return this.validations.stream().filter(e -> e.getInputName().equals(name)).findFirst().get().oldValue();
        }
        return "";
    }

    public void addValidation(Validation validation) {
        validations.add(validation);
    }


    public void validate() {
        validations.forEach(validation -> {
            try {
                validation.validate();
            } catch (SQLException throwable) {
                throwable.printStackTrace();
                throw new IllegalArgumentException("Base validator error" ,throwable);
            }
            this.errors.addAll(validation.getErrors());
        });
    }

}
