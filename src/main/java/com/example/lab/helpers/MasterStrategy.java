package com.example.lab.helpers;

import com.example.lab.dao.MySql.MySqlOrderDao;
import com.example.lab.dao.MySql.MySqlUserDao;
import com.example.lab.models.Order;
import com.example.lab.models.User;
import com.example.lab.services.OrderService;
import com.example.lab.services.ServiceFactory;

import java.sql.SQLException;
import java.util.List;

public class MasterStrategy implements RoleStrategy {

    private static final OrderService orderService;

    static {
        orderService = ServiceFactory.getInstance().getOrderService();
    }

    @Override
    public List<Order> getOrders(User user) throws SQLException {
        return orderService.getByMasterId(user.getId());
    }

    @Override
    public List<Order> getOrders(User user, int page, int itemsOnPage) throws SQLException {
        return orderService.getByMasterId(user.getId(), page, itemsOnPage);
    }

    @Override
    public int getOrdersCount(User user) throws SQLException {
        return orderService.getByMasterIdCount(user.getId());
    }

    @Override
    public String getOrderPage(User user) {
        return "order";
    }

    @Override
    public Order getOrder(User user, int orderId) throws SQLException {
        Order order = orderService.getById(orderId);

        if (order.getMasterId() != user.getId()) {
            throw new SQLException("Order not found");
        }

        return order;
    }

    @Override
    public List<User> getMasters(User user) throws SQLException {
        return ServiceFactory.getInstance().getUserService().getMastersByMaster(user);
    }
}
