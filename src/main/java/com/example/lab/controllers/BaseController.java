package com.example.lab.controllers;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.Arrays;

/**
 *
 */
public abstract class BaseController implements Controller {

    private static final Logger LOGGER = LogManager.getLogger(BaseController.class);

    /**
     * Return array numeric IDs from HttpRequest path
     *
     * @param request HttpRequest contains the client's request
     * @param count count numeric IDs in path
     *
     * @return array of IDs in path
     *
     * @throws IllegalArgumentException
     *         if the path does not contain numeric IDs or path
     *         has other count IDs than parameter {@code count}
     */
    protected Integer[] getIdsFromRequest(HttpServletRequest request, int count) throws IllegalArgumentException {
        String path = request.getPathInfo();
        String[] ids = path.replaceFirst("^[\\D]+", "").split("[\\D]+");

        if (ids.length != count || ids[0].isEmpty()) {
            LOGGER.error("Wrong IDs: " + Arrays.toString(ids));
            throw new IllegalArgumentException("Wrong item id");
        }

        return Arrays.stream(ids).map(Integer::parseInt).toArray(Integer[]::new);
    }
}
