package com.example.lab.controllers;


import jakarta.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Creates and returns {@link Controller } marked {@link ControllerAnnotation}
 */
public class ControllerFactory {

    private static final Map<String, Controller> controllers = new HashMap<>();
    private static final Logger LOGGER = LogManager.getLogger(ControllerFactory.class);

    static {
        Reflections reflection = new Reflections("com.example.lab.controllers");
        Set<Class<? extends Controller>> classes = reflection.getSubTypesOf(Controller.class);

        for (Class<?> c : classes) {
            if (c.isAnnotationPresent(ControllerAnnotation.class)) {
                try {
                    controllers.put(c.getAnnotation(ControllerAnnotation.class).value(), (Controller) c.getDeclaredConstructor().newInstance());
                } catch (ReflectiveOperationException e) {
                    LOGGER.fatal("Controller annotation fail", e);
                    throw new Error("Controller annotation fail", e);
                }
            }
        }

        LOGGER.debug("Count controllers initialized: " + controllers.size());
    }

    private ControllerFactory() {
    }

    /**
     * Return {@link Controller} instance by request path
     *
     * @param request HttpRequest contains the client's request
     *
     * @return {@link Controller} instance
     */
    public static Controller getController(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();

        pathInfo = pathInfo.replaceAll("/[\\d]+$", "/*");
        pathInfo = pathInfo.replaceAll("/[\\d]+/", "/*/");

        if (controllers.containsKey(pathInfo)) {
            return controllers.get(pathInfo);
        }

        return controllers.get("/error");
    }

}
