package com.example.lab.controllers;

import com.example.lab.exception.DBException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.sql.SQLException;


public interface Controller {

    default String execute(HttpServletRequest request, HttpServletResponse response) throws DBException {
        if (request.getMethod().equalsIgnoreCase("get")) {
            return get(request, response);
        }

        return post(request, response);
    }

    default String get(HttpServletRequest request, HttpServletResponse response) throws DBException {
        throw new UnsupportedOperationException();
    }

    default String post(HttpServletRequest request, HttpServletResponse response) throws DBException {
        throw new UnsupportedOperationException();
    }
}
