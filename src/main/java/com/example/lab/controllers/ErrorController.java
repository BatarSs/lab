package com.example.lab.controllers;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Default controller for error page
 */
@ControllerAnnotation("/error")
public class ErrorController implements Controller{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return "404";
    }
}
