package com.example.lab.utils;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.Part;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.UUID;

public class FileSaver {

    private static final int IMAGE_HEIGHT = 250;
    private static final int IMAGE_WIDTH = 250;

    public static String save(HttpServletRequest request, String name) throws IOException, ServletException {
        Part filePart = request.getPart(name);
        String fileName = Paths.get(filePart.getSubmittedFileName())
                .getFileName()
                .toString();

        String extension = "";

        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }

        fileName = UUID.randomUUID().toString() + "." + extension;

        File uploads = new File(request.getServletContext().getInitParameter("upload.location"));

        BufferedImage originalImage = ImageIO.read(filePart.getInputStream());
        BufferedImage bufferedImage = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.drawImage(originalImage, 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, null);
        graphics2D.dispose();

        File file = new File(uploads, fileName);
        ImageIO.write(bufferedImage, extension, file);

        return fileName;
    }
}
