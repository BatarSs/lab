package com.example.lab.validation.validations;

import com.example.lab.validation.ValidatorError;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidation extends AbstractValidation implements Validation{

    private final String password;

    public PasswordValidation(String inputName, String locale, String password) {
        super(inputName, locale);
        this.password = password;
    }

    @Override
    public void validate() {
        if (password == null || password.length() == 0) {
            this.errors.add(new ValidatorError("password", bundle.getString("password_required")));
            return;
        }
        String regex = "^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);
        if (!matcher.matches()) {
            this.errors.add(new ValidatorError("password", bundle.getString("invalid_password")));
        }
    }

    @Override
    public String oldValue() {
        return password;
    }
}
