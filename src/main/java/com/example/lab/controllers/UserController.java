package com.example.lab.controllers;

import com.example.lab.exception.DBException;
import com.example.lab.helpers.OrderManager;
import com.example.lab.helpers.ReviewManager;
import com.example.lab.helpers.UserManager;
import com.example.lab.dao.MySql.MySqlUserDao;
import com.example.lab.models.Order;
import com.example.lab.models.Review;
import com.example.lab.models.User;
import com.example.lab.services.ServiceFactory;
import com.example.lab.utils.Notification;
import com.example.lab.utils.Role;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.sql.SQLException;
import java.util.List;

@ControllerAnnotation("/users/*")
public class UserController extends BaseController implements Controller {

    @Override
    @AuthAnnotation(roles = {Role.MANAGER})
    public String get(HttpServletRequest request, HttpServletResponse response) throws DBException {
        Integer[] ids = getIdsFromRequest(request, 1);

        UserManager userManager = new UserManager();

        User user = userManager.getUserById(ids[0]);

        request.setAttribute("user", user);

        if(user.isMaster() || user.isUser()) {
            OrderManager orderManager = new OrderManager(user);
            List<Order> orders = orderManager.getOrders();

            request.setAttribute("orders", orders);
            ReviewManager reviewManager = new ReviewManager();

            reviewManager.getReviewsByUser(user);
            List<Review> reviews =  reviewManager.getReviewsByUser(user);

            request.setAttribute("reviews", reviews);
        }

        return "user-page";
    }

    @Override
    @AuthAnnotation(roles = {Role.MANAGER})
    public String post(HttpServletRequest request, HttpServletResponse response) throws DBException {
        Integer[] ids = getIdsFromRequest(request, 1);

        UserManager userManager = new UserManager();
        User user = userManager.getUserById(ids[0]);

        user.setBalance(Integer.parseInt(request.getParameter("balance")));

        HttpSession session = request.getSession();
        session.setAttribute("notification", new Notification(Notification.Type.SUCCESS, "User updated"));

        try {
            ServiceFactory.getInstance().getUserService().update(user);
        } catch (SQLException exception) {
            throw new DBException("User not updated", exception);
        }

        return "users/" + user.getId();
    }
}
