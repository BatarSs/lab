package com.example.lab.filters;

import com.example.lab.validation.LoginUserCheck;
import com.example.lab.validation.validators.Validator;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebFilter(urlPatterns = "/p/login")
public class LoginFilter extends HttpFilter {

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        if (req.getMethod().equals("GET")) {
            chain.doFilter(req, res);
        } else {
            Validator validator = LoginUserCheck.getValidator(req);
            validator.validate();
            if (validator.hasErrors()) {
                HttpSession session = req.getSession();
                session.setAttribute("errors", validator);
                res.sendRedirect(req.getContextPath() + "/p/login");
            } else {
                chain.doFilter(req, res);
            }
        }
    }
}
