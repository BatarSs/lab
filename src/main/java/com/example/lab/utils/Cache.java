package com.example.lab.utils;

public interface Cache {
    void add(String key, Object value, long periodInMillis);

    void remove(String key);

    Object get(String key);

    void clear();

    boolean has(String key);

    long size();
}
