package com.example.lab.helpers;

import com.example.lab.models.Review;

import java.sql.SQLException;

public class ReviewHelper {

    public static String getMark(Review review) {
        String mark;
        switch (review.getStars()) {
            case 1:
                mark = "\uD83D\uDE20";
                break;
            case 2:
                mark = "\uD83D\uDE1E";
                break;
            case 3:
                mark = "\uD83D\uDE10";
                break;
            case 5:
                mark = "\uD83D\uDE03";
                break;
            default:
                mark = "\uD83D\uDE0A";
        }

        return mark;
    }

    public static String getUserName(Review review) throws SQLException {
        return UserHelper.getUserName(review.getUserId());
    }

    public static String getMasterName(Review review) throws SQLException {
        return UserHelper.getUserName(review.getMasterId());
    }

    public static String getUserAvatar(Review review) throws SQLException {
        return UserHelper.getUserAvatar(review.getUserId());
    }
}
