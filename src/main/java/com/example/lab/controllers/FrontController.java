package com.example.lab.controllers;

import java.io.*;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Locale;

import com.example.lab.exception.DBException;
import com.example.lab.models.User;
import com.example.lab.utils.Role;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Main entry point for client requests
 */
@MultipartConfig
@WebServlet(name = "FrontController", value = "/p/*")
public class FrontController extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(FrontController.class);

    /**
     * Process get requests
     *
     * @param request
     * @param response
     * @throws ServletException in
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        Controller controller = ControllerFactory.getController(request);

        String page;

        try {
            if (hasPermissions(request, controller)) {
                page = controller.execute(request, response);
            } else {
                page = "401";
            }
        } catch (DBException exception) {
            LOGGER.error("DB exception: " + exception.getMessage(), exception);
            request.setAttribute("errorMessage", exception.getMessage());
            page = "500";
        }

        try {
            dispatch(request, response, page);
        } catch (IOException exception) {
            exception.printStackTrace();
            throw new ServletException(exception);
        }
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
        if (page.equalsIgnoreCase("json")) {
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.print(request.getAttribute("jsonData"));
            out.flush();
            return;
        }

        if (request.getMethod().equalsIgnoreCase("post")) {
            response.sendRedirect(request.getContextPath() + "/p/" + page);
        } else {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/" + page + ".jsp");
            dispatcher.forward(request, response);
        }
    }

    private boolean hasPermissions(HttpServletRequest request, Controller controller) {
        try {
            boolean hasPermission = true;
            Method method = controller.getClass()
                    .getMethod(request.getMethod().toLowerCase(new Locale("en")),
                            HttpServletRequest.class,
                            HttpServletResponse.class);

            if (method.isAnnotationPresent(AuthAnnotation.class)) {
                hasPermission = false;
                User user = (User) request.getAttribute("authUser");
                if (user != null) {
                    Role[] roles = method.getAnnotation(AuthAnnotation.class).roles();
                    if (roles.length > 0) {
                        hasPermission = Arrays.stream(roles).anyMatch(role -> user.getRole() == role);
                    } else {
                        hasPermission = true;
                    }
                }
            }

            return hasPermission;
        } catch (NoSuchMethodException exception) {
            LOGGER.error("Method not found ", exception);
            throw new UnsupportedOperationException("Method not found", exception);
        }
    }
}