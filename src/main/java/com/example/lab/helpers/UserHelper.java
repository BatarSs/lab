package com.example.lab.helpers;

import com.example.lab.dao.DaoFactory;
import com.example.lab.dao.UserDao;
import com.example.lab.models.User;
import com.example.lab.services.ServiceFactory;
import com.example.lab.services.UserService;
import com.example.lab.utils.Context;

import java.sql.SQLException;


public class UserHelper {

    private static final UserService userService;

    static {
        userService = ServiceFactory.getInstance().getUserService();
    }

    public static String getRoleName(User user) {
        return user.getRole().toString();
    }

    public static String getUserLink(int userId) {
        User user;

        try {
            user = userService.get(userId);

            if (user == null) {
                return "";
            }

            return "<a href=\"" + Context.getInstance().getContextPath() + "/p/users/" + user.getId() + "\">" +
                    "<img class=\"avatar\" src=\"" + Context.getInstance().getContextPath() + "/images/" + user.getAvatar() + "\" />" +
                    user.getName() +
                    "</a>";
        } catch (SQLException throwable) {
            return "";
        }
    }

    public static String getUserNameWithAvatar(int userId) {
        User user;

        try {
            user = userService.get(userId);

            if (user == null) {
                return "Unknown";
            }

            return "<div class=\"user-name\">" +
                    "<img class=\"avatar\" src=\"" + Context.getInstance().getContextPath() + "/images/" + user.getAvatar() + "\" />" +
                    user.getName() +
                    "</div>";
        } catch (SQLException throwable) {
            System.out.println(throwable.getMessage());
            return "Unknown";
        }
    }

    public static String getUserName(int userId) {
        User user;

        try {
            user = userService.get(userId);

            if (user == null) {
                return "Unknown";
            }

            return user.getName();
        } catch (SQLException throwable) {
            System.out.println(throwable.getMessage());
            return "Unknown";
        }
    }

    public static int getUsersCount() throws SQLException {
        return userService.getAll().size();
    }


    public static String getUserAvatar(int userId) {
        User user;

        try {
            user = userService.get(userId);

            if (user == null) {
                return "";
            }

            return user.getAvatar();
        } catch (SQLException throwable) {
            return "default.png";
        }
    }
}
