package com.example.lab.helpers;

import com.example.lab.models.Order;
import com.example.lab.models.User;
import com.example.lab.utils.OrderStatus;

import java.sql.SQLException;
import java.util.List;

public interface RoleStrategy {
    List<Order> getOrders(User user) throws SQLException;

    int getOrdersCount(User user) throws SQLException;

    List<Order> getOrders(User user, int page, int itemsOnPage) throws SQLException;

    String getOrderPage(User user);

    Order getOrder(User user, int orderId) throws SQLException;

    List<User> getMasters(User user) throws SQLException;
}
