package com.example.lab.services;

import com.example.lab.dao.DaoFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public  abstract  class ServiceFactory {
    private static ServiceFactory instance;
    protected static DaoFactory daoFactory;

    public static synchronized ServiceFactory getInstance() {
        if (instance == null) {
            try {
                Context context = new InitialContext();
                instance = (ServiceFactory) context.lookup("java:comp/env/dao/ServiceFactory");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public abstract UserService getUserService();

    public abstract OrderService getOrderService();

    public abstract ReviewService getReviewService();
}
