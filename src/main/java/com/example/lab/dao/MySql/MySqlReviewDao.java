package com.example.lab.dao.MySql;

import com.example.lab.dao.Dao;
import com.example.lab.dao.ReviewDao;
import com.example.lab.models.Review;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MySqlReviewDao extends Dao<Review> implements ReviewDao {

    private static MySqlReviewDao instance;

    public synchronized static MySqlReviewDao getInstance() {
        if (instance == null) {
            instance = new MySqlReviewDao();
        }
        return instance;
    }

    @Override
    public List<Review> getAll(Connection connection) throws SQLException {
        return getAll(connection, "SELECT r.*, o.user_id, o.master_id FROM reviews r JOIN orders o ON r.order_id=o.id ORDER BY r.id DESC");

    }

    @Override
    public List<Review> getAll(Connection connection, int page, int itemsOnPage) throws SQLException {
        return findByFields(connection, "SELECT r.*, o.user_id, o.master_id FROM reviews r JOIN orders o ON r.order_id=o.id ORDER BY r.id DESC LIMIT " + itemsOnPage + " OFFSET " + itemsOnPage * --page);
    }

    @Override
    public Review getById(Connection connection, long id) throws SQLException {
        List<Review> list = findByFields(connection,
                "SELECT r.*, o.user_id, o.master_id FROM reviews r JOIN orders o ON r.order_id=o.id WHERE id = ?",
                id);

        if (list.isEmpty()) {
            throw new SQLException("Review not found");
        }

        return list.get(0);
    }

    @Override
    public int save(Connection connection, Review review) throws SQLException {
        return save(connection,
                "INSERT INTO reviews (order_id, comment, stars) VALUES (?, ?, ?)", review);
    }

    @Override
    public void update(Connection connection, Review review) throws SQLException {
        super.update(connection,
                "UPDATE reviews SET  order_id=? ,comment=?, stars=? WHERE id = ?", review, 3, review.getId());
    }

    @Override
    public void delete(Connection connection, Review review) throws SQLException {
        super.update(connection,
                "DELETE FROM reviews WHERE id = ?", review, 1, review.getId());
    }

    @Override
    public List<Review> getByOrderId(Connection connection, Integer id, int page, int itemsOnPage) throws SQLException {
        return findByFields(connection, "SELECT r.*, o.user_id, o.master_id FROM reviews r JOIN orders o ON r.order_id=o.id WHERE order_id = ? ORDER BY r.id DESC LIMIT " + itemsOnPage + " OFFSET " + itemsOnPage * --page, id);
    }

    @Override
    public List<Review> getByOrderId(Connection connection, int id) throws SQLException {
        return findByFields(connection, "SELECT r.*, o.user_id, o.master_id FROM reviews r JOIN orders o ON o.id = r.order_id WHERE order_id = ? ORDER BY r.id DESC", id);
    }

    @Override
    public List<Review> getByMasterId(Connection connection, int id) throws SQLException {
        return findByFields(connection, "SELECT DISTINCT r.*, o.user_id, o.master_id FROM reviews r JOIN orders o ON r.order_id=o.id WHERE o.master_id = ? ORDER BY r.id DESC", id);
    }

    @Override
    public List<Review> getByUserId(Connection connection, int id) throws SQLException {
        return findByFields(connection, "SELECT DISTINCT r.*, o.user_id, o.master_id FROM reviews r JOIN orders o ON r.order_id=o.id WHERE o.user_id = ? ORDER BY r.id DESC", id);
    }

    @Override
    public int getByOrderIdCount(Connection connection, Integer id) throws SQLException {
        return countByFields(connection, "SELECT r.* FROM reviews r WHERE order_id = ?", id);
    }

    @Override
    protected Review getModelFromResult(ResultSet rs) throws SQLException {
        Review review = new Review();

        review.setId(rs.getInt("id"));
        review.setOderId(rs.getInt("order_id"));
        review.setComment(rs.getString("comment"));
        review.setStars(rs.getInt("stars"));
        review.setCreatedAt(rs.getString("created_at"));

        review.setUserId(rs.getInt("user_id"));
        review.setMasterId(rs.getInt("master_id"));

        return review;
    }

    @Override
    protected void prepareStatementFromModel(PreparedStatement ps, Review review) throws SQLException {
        ps.setLong(1, review.getOderId());
        ps.setString(2, review.getComment());
        ps.setInt(3, review.getStars());
    }
}
