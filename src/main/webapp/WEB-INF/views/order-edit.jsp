<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="userHelper" class="com.example.lab.helpers.UserHelper"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>

<t:wrapper>
    <a href="${pageContext.request.contextPath}/p/orders"
       class="btn btn-primary ms-3 mt-4"><fmt:message key="back" /></a>
    <div class="row my-5 justify-content-center">
        <form class="col col-12 col-sm-8" method="post" action="${pageContext.request.contextPath}/p/orders/${order.getId()}/edit">
            <div class="card mx-3 mb-3">
                <div class="card-body">
                    <h5 class="card-title"><fmt:message key="order" /> #${order.getId()}</h5>
                    <p class="card-text">${order.getDescription()}</p>
                </div>
                <ul class="list-group list-group-flush">

                    <div class="d-flex p-3 align-items-center">
                        <p class="mb-0"><fmt:message key="status" />: ${order.getStatus().getName(cookie['lang'].value)}</p>
                    </div>

                    <div class="d-flex p-3 align-items-center">
                        <p class="mb-0"><fmt:message key="OVERDUE" />: ${order.getExpireDate()}</p>
                    </div>

                    <c:choose>
                        <c:when test="${authUser.isManager()}">
                            <div class="d-flex p-3 align-items-center">
                                <label for="master-select" class="form-label me-3"><fmt:message key="master" /></label>
                                <select class="form-select" id="master-select" name="master_id" aria-label="Default select example">
                                    <c:forEach var="master" items="${masters}">
                                        <option <c:if test="${order.getMasterId() == master.getId()}"> selected </c:if> value="${master.getId()}">${master.getName()}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item"><fmt:message key="master" />: ${userHelper.getUserLink(order.getMasterId())}</li>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${authUser.isManager()}">
                            <div class="d-flex p-3 align-items-center">
                                <label for="master-select" class="form-label me-3"><fmt:message key="price" /></label>
                                <input type="number" class="form-control" value="${order.getPrice()}" name="price">
                            </div>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">${order.getPrice()}</li>
                        </c:otherwise>
                    </c:choose>
                    <li class="list-group-item">${order.getCreatedAt()}</li>
                </ul>
                <div class="card-body">
                    <c:choose>
                        <c:when test="${authUser.isUser()}">
                            <div class="d-flex p-3 align-items-center gap-3">
                                <c:if test="${order.getStatus() == 'WAITPAID'}">
                                    <button class="btn btn-success action-btn" data-status="PAID"><fmt:message key="PAID" /></button>
                                    <button class="btn btn-danger action-btn" data-status="CANCEL"><fmt:message key="CANCEL" /></button>
                                </c:if>
                                <c:if test="${order.getStatus() == 'WAIT' || order.getStatus() == 'NEW'}">
                                    <button class="btn btn-danger action-btn" data-status="CANCEL"><fmt:message key="CANCEL" /></button>
                                </c:if>
                            </div>
                        </c:when>
                        <c:when test="${authUser.isManager()}">
                            <div class="d-flex p-3 align-items-center gap-3">
                                <c:if test="${order.getStatus() == 'WAITPAID'}">
                                    <button class="btn btn-danger action-btn" data-status="CANCEL"><fmt:message key="CANCEL" /></button>
                                </c:if>
                                <c:if test="${order.getStatus() == 'WAIT' || order.getStatus() == 'NEW'}">
                                    <button class="btn btn-danger action-btn" data-status="WAITPAID"><fmt:message key="WAITPAID" /></button>
                                    <button class="btn btn-danger action-btn" data-status="CANCEL"><fmt:message key="CANCEL" /></button>
                                </c:if>
                            </div>
                        </c:when>
                        <c:when test="${authUser.isMaster()}">
                            <div class="d-flex p-3 align-items-center gap-3">
                                <c:if test="${order.getStatus() == 'PAID'}">
                                    <button class="btn btn-danger action-btn" data-status="WORK"><fmt:message key="WORK" /></button>
                                    <button class="btn btn-success action-btn" data-status="DONE"><fmt:message key="DONE" /></button>
                                </c:if>
                                <c:if test="${order.getStatus() == 'WORK'}">
                                    <button class="btn btn-danger action-btn" data-status="DONE"><fmt:message key="DONE" /></button>
                                </c:if>
                            </div>
                        </c:when>
                    </c:choose>
                </div>
                <div class="card-body d-flex justify-content-end">
                    <button class="btn btn-primary"><fmt:message key="save" /></button>
                </div>
            </div>
        </form>
    </div>
</t:wrapper>
