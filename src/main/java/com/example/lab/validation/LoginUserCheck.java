package com.example.lab.validation;

import com.example.lab.validation.validations.*;
import com.example.lab.validation.validators.BaseValidator;
import com.example.lab.validation.validators.Validator;
import jakarta.servlet.http.HttpServletRequest;

public class LoginUserCheck {

    public static Validator getValidator(HttpServletRequest request) {
        Validator loginValidator = new BaseValidator();

        String locale = request.getAttribute("lang").toString();

        loginValidator.addValidation(new ValidEmailValidation("email", locale, request.getParameter("email")));
//        loginValidator.addValidation(new PasswordValidation("password", request.getParameter("password")));
        loginValidator.addValidation(new LoginValidation("email", locale, request.getParameter("email"), request.getParameter("password")));

        return loginValidator;
    }
}
