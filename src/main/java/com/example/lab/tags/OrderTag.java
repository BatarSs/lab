package com.example.lab.tags;

import com.example.lab.helpers.UserHelper;
import com.example.lab.models.Order;
import com.example.lab.models.User;
import com.example.lab.utils.OrderStatus;
import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.JspWriter;
import jakarta.servlet.jsp.PageContext;
import jakarta.servlet.jsp.tagext.SimpleTagSupport;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;


public class OrderTag extends SimpleTagSupport {

    private Order order;
    private User user;
    private String classes = "";
    private String locale = "";
    private boolean showEdit = false;
    private boolean showView = false;
    private  ResourceBundle bundle;


    public void setUser(User user) {
        this.user = user;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public void setLocale(String locale) {
        this.locale = locale;
        this.bundle = ResourceBundle.getBundle("pages", Locale.forLanguageTag(locale));
    }

    public void setClasses(String order) {
        this.classes = order;
    }

    public void setShowEdit(boolean showEdit) {
        this.showEdit = showEdit;
    }

    public void setShowView(boolean showView) {
        this.showView = showView;
    }

    @Override
    public void doTag() throws IOException {
        PageContext pageContext = (PageContext) getJspContext();
        JspWriter out = pageContext.getOut();
        StringBuilder sb = new StringBuilder();
        sb.append("<div");

        if (classes.length() > 0) {
            sb.append(" class=\"").append(classes).append("\" ");
        }

        sb.append(">\n");
        sb.append("<div class=\"card mx-3 mb-3\">\n" +
                "                    <div class=\"card-body\">\n");
        sb.append("<h5 class=\"card-title\">").append(bundle.getString("order")).append(" #").append(order.getId()).append("</h5>");
        sb.append("<p class=\"card-text\">").append(order.getDescription()).append(" </p>");
        sb.append("</div>");
        sb.append(this.getDescription());
        sb.append(this.addLinks(pageContext));
        sb.append("                </div>\n" +
                "            </div>");
        out.print(sb.toString());
    }

    private String addLinks(PageContext pageContext) {
        String buttons = "<div class=\"card-body d-flex flex-row-reverse gap-2\">\n";

        if (showView) {
            buttons += "<a href=\"" + pageContext.getRequest().getServletContext().getContextPath() + "/p/orders/" + order.getId() + "\" class=\"btn btn-warning\">" + bundle.getString("view") + "</a>\n";
        }

        if (showEdit) {
            buttons += "<a href=\"" + pageContext.getRequest().getServletContext().getContextPath() + "/p/orders/" + order.getId() + "/edit\"\n class=\"btn btn-primary\">" + bundle.getString("edit") + "</a>\n";
        }

        if (user.isUser() && order.getStatus() == OrderStatus.DONE) {
            buttons += "<a href=\"" + pageContext.getRequest().getServletContext().getContextPath() + "/p/order/reviews/" + order.getId() + "\"\n class=\"btn btn-success\">" + bundle.getString("review") + "</a>\n";
        }

        buttons += "</div>";

        return buttons;
    }


    private String getDescription() {
        String description = "<ul class=\"list-group list-group-flush\">\n";

        description += getDescriptionItem(bundle.getString("status"), order.getStatus().getName(locale));
        description += getDescriptionItem(bundle.getString("price"), String.valueOf(order.getPrice()));
        description += getDescriptionItem(bundle.getString("OVERDUE"), String.valueOf(order.getExpireDate()));

        if (user.isMaster()) {
            description += getDescriptionItem(bundle.getString("user"), UserHelper.getUserLink(order.getUserId()));
        }

        if (user.isUser()) {
            description += getDescriptionItem(bundle.getString("master"), UserHelper.getUserLink(order.getMasterId()));
        }

        if (user.isManager()) {
            description += getDescriptionItem(bundle.getString("user"), UserHelper.getUserLink(order.getUserId()));
            description += getDescriptionItem(bundle.getString("master"), UserHelper.getUserLink(order.getMasterId()));
        }

        description += getDescriptionItem(bundle.getString("created"), order.getCreatedAt());
        description += "</ul>";

        return description;
    }

    private String getDescriptionItem(String name, String value) {
        return "<li class=\"list-group-item\">" + name + ": " + value + "</li>";
    }
}
