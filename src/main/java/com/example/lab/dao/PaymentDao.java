package com.example.lab.dao;

import com.example.lab.models.Payment;
import com.example.lab.utils.Database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PaymentDao extends Dao<Payment> {

    public int save(Payment payment) throws SQLException {
        return save(Database.INSTANCE.openConnection(),
                "INSERT INTO payments (amount, order_id) VALUES (?, ?)",
                payment);
    }

    @Override
    protected Payment getModelFromResult(ResultSet rs) throws SQLException {
        Payment payment = new Payment();

        payment.setId(rs.getInt("id"));
        payment.setOrderId(rs.getInt("order_id"));
        payment.setAmount(rs.getInt("amount"));
        payment.setCreatedAt(rs.getString("created_at"));

        return payment;
    }

    @Override
    protected void prepareStatementFromModel(PreparedStatement ps, Payment payment) throws SQLException {
        ps.setInt(1, payment.getAmount());
        ps.setInt(2, payment.getOrderId());
    }
}
