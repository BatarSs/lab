<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="pages"/>
<t:wrapper>
   <div class="form-signin">
       <form method="post" action="${pageContext.request.contextPath}/p/login">
           <img class="mb-4" src="${pageContext.request.contextPath}/resources/images/logo.png" alt="" width="72" height="57">
           <h1 class="h3 mb-3 fw-normal"><fmt:message key="login" /></h1>


           <c:if test="${errors.hasErrorsByName('email')}">
               <c:forEach var="error" items="${errors.getErrorsByName('email')}">
                   <p><c:out value="${error.getDescription()}" /></p>
               </c:forEach>
           </c:if>
           <c:choose>
               <c:when test="${errors.hasErrors()}">
                   <div class="form-floating">
                       <input name="email" type="email" class="form-control" value="${errors.getOldByName('email')}" id="floatingInput" placeholder="name@example.com">
                       <label for="floatingInput"><fmt:message key="email" /></label>
                   </div>
               </c:when>
               <c:otherwise>
                   <div class="form-floating">
                       <input name="email" type="email" class="form-control" value="asd@asd.asd" id="floatingInput" placeholder="name@example.com">
                       <label for="floatingInput"><fmt:message key="email" /></label>
                   </div>
               </c:otherwise>
           </c:choose>


           <div class="form-floating">
               <input name="password" type="password" value="123123" class="form-control" id="floatingPassword" placeholder="<fmt:message key="password" />">
               <label for="floatingPassword"><fmt:message key="password" /></label>
           </div>

<%--           <div class="checkbox mb-3">--%>
<%--               <label>--%>
<%--                   <input type="checkbox" value="remember-me"> Remember me--%>
<%--               </label>--%>
<%--           </div>--%>
           <button class="w-100 btn btn-lg btn-primary" type="submit"><fmt:message key="sign_in" /></button>
       </form>
   </div>
</t:wrapper>
